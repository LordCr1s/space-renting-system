from django.contrib import admin
from .models import *

@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    list_display = ('place_type', 'location', 'uploaded_on', 'status')
    search_fields = ('place_type', 'location', )

@admin.register(LivingPlace)
class LivingPlaceAdmin(admin.ModelAdmin):
    list_display = ('place', 'price', 'rooms', 'electricity', 'water')

@admin.register(Office)
class OfficeAdmin(admin.ModelAdmin):
    list_display = ('place', 'rooms', 'square_meters', 'price')

@admin.register(Hall)
class HallAdmin(admin.ModelAdmin):
    list_display = ('place', 'square_meters', 'price')

@admin.register(PlaceImage)
class PlaceImageAdmin(admin.ModelAdmin):
    list_display = ('place', )

@admin.register(RentRequest)
class RentRequestAdmin(admin.ModelAdmin):
    list_display =('sender', 'receiver', 'place')
    
@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    list_display = ('renter', 'owner', 'place', 'created_at')

@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('contract', 'duration', 'amount', 'payed_at')
