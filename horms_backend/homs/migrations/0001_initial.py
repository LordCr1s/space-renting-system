# Generated by Django 2.1.7 on 2019-03-13 03:43

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Hall',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('square_meters', models.CharField(max_length=30)),
                ('parcking_place', models.IntegerField(choices=[(1, 'Available'), (0, 'Not available')], default=1)),
                ('descritpion', models.TextField()),
                ('price', models.CharField(max_length=10)),
                ('payment_terms', models.CharField(max_length=300)),
            ],
            options={
                'verbose_name': 'Hall',
                'verbose_name_plural': 'Halls',
            },
            managers=[
                ('halls', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='LivingPlace',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rooms', models.IntegerField(default=1)),
                ('master_rooms', models.IntegerField(default=1)),
                ('bathrooms', models.IntegerField(default=0)),
                ('electricity', models.IntegerField(choices=[(1, 'Available'), (0, 'Not available')], default=1)),
                ('water', models.IntegerField(choices=[(1, 'Available'), (0, 'Not available')], default=1)),
                ('parcking_place', models.IntegerField(choices=[(1, 'Available'), (0, 'Not available')], default=1)),
                ('descritpion', models.TextField()),
                ('price', models.CharField(max_length=10)),
                ('payment_terms', models.CharField(max_length=300)),
            ],
            options={
                'verbose_name': 'Living place',
                'verbose_name_plural': 'Living places',
            },
            managers=[
                ('places', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rooms', models.IntegerField(default=1)),
                ('square_meters', models.CharField(max_length=30)),
                ('parcking_place', models.IntegerField(choices=[(1, 'Available'), (0, 'Not available')], default=1)),
                ('descritpion', models.TextField()),
                ('price', models.CharField(max_length=10)),
                ('payment_terms', models.CharField(max_length=300)),
            ],
            options={
                'verbose_name': 'Office',
                'verbose_name_plural': 'Offices',
            },
            managers=[
                ('offices', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('location', models.CharField(max_length=255)),
                ('location_description', models.TextField()),
                ('place_type', models.IntegerField(choices=[(1, 'House'), (2, 'Appartment'), (3, 'Office'), (4, 'Hall'), (0, 'Choose place type')], default=0)),
                ('uploaded_on', models.DateField(auto_now=True)),
                ('status', models.CharField(choices=[(0, 'Choose place status'), (1, 'New construction'), (2, 'Good Condition'), (3, 'Old')], default=0, max_length=30)),
                ('construction_year', models.CharField(max_length=5)),
                ('user', models.ManyToManyField(related_name='places', related_query_name='place', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Place',
                'verbose_name_plural': 'Places',
            },
            managers=[
                ('places', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='PlaceImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('place', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='homs.Place')),
            ],
            options={
                'verbose_name': 'Image of a place',
                'verbose_name_plural': 'Images of a place',
            },
            managers=[
                ('images', django.db.models.manager.Manager()),
            ],
        ),
        migrations.AddField(
            model_name='office',
            name='place',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='homs.Place'),
        ),
        migrations.AddField(
            model_name='livingplace',
            name='place',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='living_place', to='homs.Place'),
        ),
        migrations.AddField(
            model_name='hall',
            name='place',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='homs.Place'),
        ),
        migrations.AddIndex(
            model_name='placeimage',
            index=models.Index(fields=['place'], name='homs_placei_place_i_37fde6_idx'),
        ),
        migrations.AddIndex(
            model_name='place',
            index=models.Index(fields=['title'], name='homs_place_title_2fcde5_idx'),
        ),
        migrations.AddIndex(
            model_name='place',
            index=models.Index(fields=['place_type'], name='homs_place_place_t_fcb769_idx'),
        ),
        migrations.AddIndex(
            model_name='office',
            index=models.Index(fields=['place'], name='homs_office_place_i_e6887c_idx'),
        ),
        migrations.AddIndex(
            model_name='livingplace',
            index=models.Index(fields=['place'], name='homs_living_place_i_8ae63e_idx'),
        ),
        migrations.AddIndex(
            model_name='hall',
            index=models.Index(fields=['place'], name='homs_hall_place_i_545741_idx'),
        ),
    ]
