# Generated by Django 2.2.1 on 2019-05-06 05:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homs', '0007_auto_20190313_0735'),
    ]

    operations = [
        migrations.CreateModel(
            name='JoanHostel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('location', models.CharField(max_length=255)),
                ('number_of_rooms', models.IntegerField(default=0)),
            ],
        ),
    ]
