# Generated by Django 2.2.1 on 2019-06-16 06:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homs', '0014_auto_20190616_0558'),
    ]

    operations = [
        migrations.AddField(
            model_name='rentrequest',
            name='reply_status',
            field=models.CharField(choices=[('waiting', 'waiting'), ('rejected', 'rejected'), ('accepted', 'accepted')], default='waiting', max_length=10),
        ),
    ]
