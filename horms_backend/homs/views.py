from django.shortcuts import render
from homs.models import *
from accounts.models import CustomUser as User
from rest_framework import generics, serializers
from .serializers import PlaceSerializer, RequestSerializer, ContractSerializer, PaymentsSerializer
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view


@api_view(['GET', ])
def getPlaces(request):
    serializer = PlaceSerializer(Place.places.all(), many=True)
    return Response(serializer.data)

@api_view(['GET', ])
def getPlacesbyuser(request, user_id):

    # find the specified user
    try:
        user = User.users.get(email=user_id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = PlaceSerializer(Place.places.filter(user=user), many=True)
    return Response(serializer.data)

@api_view(['GET', ])
def get_request(request, user_id):
    
    # find the specified user
    try:
        user = User.users.get(email=user_id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # formulate queryset
    requests_as_sender = RentRequest.requests.filter(sender=user, reply_status="waiting")
    requests_as_receiver = RentRequest.requests.filter(receiver=user, reply_status="waiting")
    all_requests = (requests_as_sender | requests_as_receiver).order_by('-sent_at')

    serializer = RequestSerializer(all_requests, many=True)
    return Response(status=status.HTTP_200_OK, data=serializer.data)


@api_view(['POST', ])
def rent_a_place(request):

    # find user with the provided email
    try:
        user = User.users.get(email=request.data['user_email'])
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    print(request.data)
    # find a place to rent
    try:
        place = Place.places.get(pk=request.data['place_id'])
    except Place.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    # make a request 
    request = RentRequest.requests.create(
        sender = user,
        receiver = place.user,
        place = place
    )
    request.save()
    return Response(status=status.HTTP_200_OK)


@api_view(['POST', ])
def modify_request(request):

    # find request with the given id
    try:
        rent_request = RentRequest.requests.get(pk=request.data['request_id'])
    except RentRequest.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # modify request
    rent_request.reply_status = request.data['reply']
    rent_request.save()
    return Response(status=status.HTTP_200_OK)

@api_view(['GET', ])
def get_contracts(request, user_id):

    # get user with provided id
    try:
        user = User.users.get(email=user_id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # find contracts with this user and merge them
    contract_as_renter = Contract.contracts.filter(renter=user)
    contract_as_owner = Contract.contracts.filter(owner=user)
    all_contracts = (contract_as_owner | contract_as_renter).order_by('-created_at')

    # give back contracts
    serializer = ContractSerializer(all_contracts, many=True)
    return Response(status=status.HTTP_200_OK, data=serializer.data)

@api_view(['POST', ])
def add_payment(request):

    # find the contract that the payment is added for
    try:
        contract = Contract.contracts.get(pk=request.data['contract_id'])
    except Contract.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    # create payment record
    payment = Payment.payments.create(
        contract = contract,
        amount = request.data['amount'],
        duration = request.data['duration']
    )
    payment.save()

    # return serialized payment record
    serializer = PaymentsSerializer(payment)
    return Response(status=status.HTTP_200_OK, data=serializer.data)

@api_view(['POST', ])
def cancel_payment(request):

    # find payment record with the provided id
    try:
        payment = Payment.payments.get(pk=request.data['payment_id'])
    except Payment.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    # modify and save the payment record
    payment.status = 'cancelled'
    payment.save()
    return Response(status=status.HTTP_200_OK)