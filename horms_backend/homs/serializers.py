from rest_framework import serializers
from rest_framework.response import Response
from .models import *
from accounts.models import *


class LivingPlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = LivingPlace
        fields = "__all__"
    
class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = "__all__"

class HallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hall
        fields = "__all__"

class PlaceImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceImage
        fields = "__all__"

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):

    userprofile = UserProfileSerializer()

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'email', 'mobile_number', 'userprofile')

class PlaceSerializer(serializers.ModelSerializer):
    living_place = LivingPlaceSerializer()
    office = OfficeSerializer()
    hall = HallSerializer()
    user = UserSerializer()
    place_images = PlaceImageSerializer(many=True)

    class Meta:
        model = Place 
        fields = '__all__'


class RequestSerializer(serializers.ModelSerializer):

    sender = UserSerializer()
    receiver = UserSerializer()
    place = PlaceSerializer()

    class Meta:
        model = RentRequest
        fields = '__all__'


class PaymentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'

class ContractSerializer(serializers.ModelSerializer):

    owner = UserSerializer()
    renter = UserSerializer()
    place = PlaceSerializer()
    payments = PaymentsSerializer(many=True)

    class Meta:
        model = Contract
        fields = '__all__'