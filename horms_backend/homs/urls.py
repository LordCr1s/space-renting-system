from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from .models import Place, LivingPlace, Office, Hall, PlaceImage


urlpatterns = [
    path('api/places', views.getPlaces),
    path('api/places/<str:user_id>', views.getPlacesbyuser),
    path('api/requests/<str:user_id>', views.get_request),
    path('api/rentplace', views.rent_a_place),
    path('api/modifyrequest', views.modify_request),
    path('api/getcontracts/<str:user_id>', views.get_contracts),
    path('api/addpayment', views.add_payment),
    path('api/modifypayment', views.cancel_payment)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)