from django.db import models
from horms_backend import settings
from django.urls import reverse

class Place(models.Model):
    
    CHOOSE_TYPE = 0
    HOUSE = 1
    APPARTMENT = 2
    OFFICE = 3
    HALL = 4

    PLACE_TYPE = [  (HOUSE, 'House'), (APPARTMENT, 'Appartment'), (OFFICE, 'Office'),
                    (HALL, 'Hall'), (CHOOSE_TYPE, 'Choose place type') ]

    NEW_CONSTRUCTION = 1
    GOOD_CONDITION = 2
    OLD = 3

    PLACE_STATUS = [  (CHOOSE_TYPE, 'Choose place status'), (NEW_CONSTRUCTION, 'New construction'), 
                      (GOOD_CONDITION, 'Good Condition'), (OLD, 'Old')]

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name = "places",
        related_query_name = "place",
    )
    title = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    location_description = models.TextField()
    place_type = models.IntegerField(choices=PLACE_TYPE, default=CHOOSE_TYPE)
    uploaded_on = models.DateField(auto_now=True, editable=False)
    status = models.IntegerField(choices=PLACE_STATUS, default=CHOOSE_TYPE)
    construction_year = models.CharField(max_length=5)
    cover_photo = models.ImageField(upload_to="images/place/%Y/%M/%d")

    places = models.Manager()

    class Meta:
        verbose_name = "Place"
        verbose_name_plural = "Places"
        indexes = [
            models.Index(fields=['user']),
            models.Index(fields=['title']),
            models.Index(fields=['place_type']),
        ]

    def __str__(self):
        return self.title


def availability_choices():

    AVAILABLE = 'available'
    NOT_AVAILABLE = 'unavailable'
    AVAILABILITY =[(AVAILABLE, 'Available'), (NOT_AVAILABLE, 'Not available')]

    return AVAILABILITY

class LivingPlace(models.Model):

    place = models.OneToOneField(Place, on_delete=models.CASCADE, related_name="living_place")
    rooms = models.IntegerField(default=1)
    master_rooms = models.IntegerField(default=1)
    bathrooms = models.IntegerField(default=0)
    electricity = models.CharField(max_length=255, choices=availability_choices(), default='available')
    water = models.CharField(max_length=255, choices=availability_choices(), default='available')
    parcking_place = models.CharField(max_length=255, choices=availability_choices(), default='available')
    descritpion = models.TextField()
    price = models.CharField(max_length=255)
    payment_terms = models.CharField(max_length=300)

    places = models.Manager()

    class Meta:
        verbose_name = "Living place"
        verbose_name_plural = "Living places"
        indexes = [
            models.Index(fields=['place'])
        ]

    def __str__(self):
        return self.price

    def get_absolute_url(self):
        return reverse("Living_place_detail", kwargs={"pk": self.pk})


class Office(models.Model):

    place = models.OneToOneField(Place, on_delete=models.CASCADE, related_name="office")
    rooms = models.IntegerField(default=1)
    square_meters = models.CharField(max_length=30)
    parcking_place = models.CharField(max_length=255, choices=availability_choices(), default='available')
    descritpion = models.TextField()
    price = models.CharField(max_length=255)
    payment_terms = models.CharField(max_length=300)

    offices = models.Manager()

    class Meta:
        verbose_name = "Office"
        verbose_name_plural = "Offices"
        indexes = [
            models.Index(fields=['place'])
        ]

    def __str__(self):
        return self.place.title

    def get_absolute_url(self):
        return reverse("Office_detail", kwargs={"pk": self.pk})


class Hall(models.Model):

    place = models.OneToOneField(Place, on_delete=models.CASCADE, related_name="hall")
    square_meters = models.CharField(max_length=30)
    parcking_place = models.CharField(max_length=255, choices=availability_choices(), default='available')
    descritpion = models.TextField()
    price = models.CharField(max_length=255)
    payment_terms = models.CharField(max_length=300)   

    halls = models.Manager()

    class Meta:
        verbose_name = "Hall"
        verbose_name_plural = "Halls"
        indexes = [
            models.Index(fields=['place'])
        ]

    def __str__(self):
        return self.place.title

    def get_absolute_url(self):
        return reverse("Hall_detail", kwargs={"pk": self.pk})


class PlaceImage(models.Model):

    place = models.ForeignKey(Place, on_delete=models.CASCADE, related_name="place_images")
    image = models.ImageField(upload_to="images/place/%Y/%M/%d")

    images = models.Manager()
    
    class Meta:
        verbose_name = "Image of a place"
        verbose_name_plural = "Images of a place"
        indexes = [
            models.Index(fields=['place'])
        ]

    def __str__(self):
        return self.place.title

    def get_absolute_url(self):
        return reverse("PlaceImage_detail", kwargs={"pk": self.pk})

class RentRequest(models.Model):

    WAITING = 'waiting'
    REJECTED = 'rejected'
    ACCEPTED = 'accepted'
    CANCELLED = 'cancelled'

    REPLY_STATUS = [
        (WAITING, 'waiting'),
        (REJECTED, 'rejected'),
        (ACCEPTED, 'accepted'),
        (CANCELLED, 'cancelled')
    ]

    sender = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='request_sender')
    receiver = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='request_receiver')
    place = models.ForeignKey(Place, on_delete=models.CASCADE, related_name='request')
    sent_at = models.DateTimeField(auto_now=True)
    reply_status = models.CharField(max_length=10, choices=REPLY_STATUS, default=WAITING)

    requests = models.Manager()

    def __str__(self):
        return self.sender.get_full_name()

class Contract(models.Model):

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="place_owner")
    renter = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="place_renter")
    place = models.ForeignKey(Place, on_delete=models.CASCADE, related_name='place')
    created_at = models.DateField(auto_now=True)

    contracts = models.Manager()

    def __str__(self):
        return self.owner.get_username()

class Payment(models.Model):

    ACTIVE = 'active'
    CANCELLED = 'cancelled'
    PAYMENT_STATUS = [(ACTIVE, 'active'), (CANCELLED, 'cancelled')]

    contract = models.ForeignKey(Contract, related_name='payments', on_delete=models.CASCADE)
    amount = models.CharField(max_length=12)
    payed_at = models.DateField(auto_now=True)
    duration = models.CharField(max_length=255)
    status = models.CharField(max_length=10, choices=PAYMENT_STATUS, default=ACTIVE)

    payments = models.Manager()

    def __str__(self):
        return self.amount