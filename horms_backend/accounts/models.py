from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from time import strftime, gmtime
from django.utils import timezone
from django.conf import settings
from ckeditor.fields import RichTextField

class CustomManager(BaseUserManager):
    def _create_user(self, email=None, password=None, is_superuser=False, is_staff=False, is_active=False):
        if email is None:
            raise ValueError("users must have an email")
        if password is None:
            raise ValueError("users must have a password")

        user = self.model(
            email=self.normalize_email(email),
            is_superuser=is_superuser,
            is_staff=is_staff,
            is_active=is_active,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **kwargs):
        user = self._create_user(email, password, is_staff=False, is_superuser=False, is_active=False)
        user.first_name = kwargs['first_name']
        user.last_name = kwargs['last_name']
        # user.user_role = kwargs['user_role']
        user.date_joined = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.last_login = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.save(using=self._db)
        return user

    def create_staffuser(self, email=None, password=None, **kwargs):
        user = self._create_user(email, password, is_staff=True, is_superuser=False, is_active=False)
        user.first_name = kwargs['first_name']
        user.last_name = kwargs['last_name']
        # user.user_role = kwargs['user_role']
        user.date_joined = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.last_login = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **kwargs):
        user = self._create_user(email, password, is_superuser=True, is_staff=True, is_active=True)
        user.first_name = kwargs['first_name']
        user.last_name = kwargs['last_name']
        # user.user_role = kwargs['user_role']
        user.date_joined = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.last_login = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):

    # admin = 0
    # mpangishaji = 1
    # mpangaji = 2
    # user_type = [(admin, 'admin'), (mpangaji, 'mpangaji'), (mpangishaji, 'mpangishaji')]

    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)
    email = models.EmailField(unique=True)
    mobile_number = models.CharField(max_length=15)
    # user_role = models.IntegerField(choices=user_type, default=1)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    last_login = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'mobile_number']

    users = CustomManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.first_name

    def get_short_name(self):
        return self.last_name

    def get_username(self):
        return self.email

    @staticmethod
    def has_perm(obj=None):
        return True

    @staticmethod
    def has_module_perms(app_label):
        return True


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    profile_picture = models.ImageField(upload_to='users/%Y/%m/%d', blank=True, null=True)
    bio = RichTextField(blank=True, null=True)
    activation_key = models.IntegerField(blank=True, null=True)
    activation_key_expirity = models.DateTimeField(blank=True, null=True)

    users = models.Manager()

    def __str__(self):
        return self.user.email

