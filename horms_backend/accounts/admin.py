from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserAdminCreationForm, UserAdminChangeForm
from .models import *


class UserAdmin(BaseUserAdmin):

    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # fields that will be displayed on the users list
    list_display = ('first_name', 'email', 'last_name', 'last_login', 'date_joined', )

    # fields that will be used to filter the users on the users list
    list_filter = ('first_name', 'last_name', 'date_joined', 'last_login')

    # fields that will be editable when user info has to be updated
    fieldsets = (
        ('Login Credentials', {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_staff', 'is_superuser', 'is_active', 'groups')}),
    )

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}),
    )

    # this creates the search  form that will search users by their info
    search_fields = ('email', 'first_name', 'last_name')

    # list of users will be ordered by their last login time
    ordering = ('last_login',)


# register our custom user model and user admin model
admin.site.register(CustomUser, UserAdmin)

class ProfileAdmin(admin.ModelAdmin):

    # fields that will be displayed on the users list
    list_display = ('user', 'profile_picture',)

    # fields that will be used to filter the users on the users list
    list_filter = ('user',)

admin.site.register(UserProfile, ProfileAdmin)


## admin page customizations
admin.sites.AdminSite.site_header = "Hors Administration"
admin.sites.AdminSite.site_title = "Hors admin"