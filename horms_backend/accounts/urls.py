from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('api/register', views.custom_user_register),
    path('api/activate/<int:id>', views.confirm_account),
    path('api/login', views.user_login),
    path('api/logout', views.user_logout),
    path('api/resetmypassword', views.reset_password_email),
    path('api/resetmypassword/<int:id>', views.reset_password),
    # path('updatepassword/<int:pk>', views.update_password)
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)