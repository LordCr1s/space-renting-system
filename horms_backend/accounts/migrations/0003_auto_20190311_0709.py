# Generated by Django 2.1.7 on 2019-03-11 07:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_delete_systeminfo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='user_role',
            field=models.IntegerField(choices=[(0, 'admin'), (2, 'mpangaji'), (1, 'mpangishaji')], default=1),
        ),
    ]
