from django.shortcuts import get_object_or_404
from . import forms
from django.contrib.auth import authenticate, login, views
from django.core.mail import send_mail
import random, datetime
from django.utils import timezone
from .models import UserProfile, CustomUser
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.views import APIView
from django.contrib.auth import logout

from rest_framework import serializers
from rest_framework.response import Response

class RegistrationFormSerilizer(serializers.Serializer):
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)
    email = serializers.EmailField()
    mobile_number = serializers.CharField(max_length=255)
    password1 = serializers.CharField(max_length=255)
    password2 = serializers.CharField(max_length=255)

@api_view(['POST', ])
def custom_user_register(request):
    serializer = RegistrationFormSerilizer(request.data)
    form = forms.RegisterForm(serializer.data)
    if form.is_valid():
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password2'])
        user.save()

        # creating activation key and save it together with the user
        x = datetime.datetime.today() # x is to differentiate between keys of unactivated users
        activation_key = random.randint(1, 1000) + x.year + x.month + x.day + x.hour + x.minute + x.second
        key_expires = datetime.datetime.today() + datetime.timedelta(2)

        new_user = UserProfile(
                        user=user,
                        activation_key=activation_key,
                        activation_key_expirity=key_expires)
        new_user.save()

        email_subject = 'Your new site account confirmation'
        email_body = """ Thank you for signing up with us %s \n\n 
                        here is your verification code \t\t%s """ % (
                        form.cleaned_data['email'],
                        new_user.activation_key)
        send_mail(email_subject,
                  email_body,
                  'miniacyclo@gmail.com',
                  [form.cleaned_data['email']])
        return Response(status=status.HTTP_201_CREATED)
    else:
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['GET', ])
def confirm_account(request, id):
    try:
        inactive_user = UserProfile.users.get(activation_key=id)
    except UserProfile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if inactive_user is not None:
        current_time = timezone.now()
        if inactive_user.activation_key_expirity < current_time:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        user = inactive_user.user
        user.is_active = True
        inactive_user.activation_key += random.randint(2, 1000000)
        inactive_user.save()
        user.save()
        return Response(status=status.HTTP_202_ACCEPTED)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


# def get_serializer(serialized_model, **kwargs):

#     class ModalSerializer(serializers.ModelSerializer):

#         class Meta:
#             model = serialized_model
#             fields = kwargs['fields']
#             # read_only_fields = kwargs['rof']
#     return ModalSerializer

# def serialize_queryset(data, serializer):
#     if not data:
#         return Response(status=status.HTTP_404_NOT_FOUND)

#     serialized_data = serializer(data, many=True)
#     return Response(serialized_data.data)


@api_view(['POST', ])
def reset_password_email(request):

    class PasswordResetSerializer(serializers.Serializer):
        email = serializers.EmailField()

    serializer = PasswordResetSerializer(request.data)
    form = forms.PasswordReset(serializer.data)

    if form.is_valid():
        email = form.cleaned_data['email']
        try:
            user = CustomUser.users.get(email=email)
        except CustomUser.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if user is not None:

            # creating activation key and save it together with the user
            x = datetime.datetime.today() # x is to differentiate between keys of unactivated users
            activation_key = random.randint(1, 1000) + x.year + x.month + x.day + x.hour + x.minute + x.second
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            try:
                new_user = UserProfile.users.get(user=user)
            except UserProfile.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

            new_user.activation_key = activation_key
            new_user.activation_key_expirity = key_expires
            new_user.save()

            email_subject = 'password reset form'
            email_body = """ Thank you for signing up with us %s \n\n 
                        here is your password reset code \t\t%s """ % (
                user.email,
                activation_key)

            send_mail(email_subject,
                      email_body,
                      'miniacyclo@gmail.com',
                      [form.cleaned_data['email']])
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST', ])
def reset_password(request, id):
    try:
        user = UserProfile.users.get(activation_key=id)
    except UserProfile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if user is not None:
        current_time = timezone.now()
        if user.activation_key_expirity < current_time:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        new_user = user.user
        if request.method == 'POST':
            form = forms.Newpassword(request.POST)
            if form.is_valid():
                new_user.set_password(form.cleaned_data['password2'])
                user.activation_key += random.randint(2, 1000000)
                user.save()
                new_user.save()
                return Response(status=status.HTTP_202_ACCEPTED)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


# @api_view(['POST', ])
# def update_password(request, pk):

#     try:
#         user = CustomUser.users.get(id=pk)
#     except CustomUser.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
    
#     if request.method == 'POST':
#         form = forms.Newpassword(request.POST)
#         if form.is_valid():
#             try:
#                 logout(request)
#             except:
#                 return Response(status=status.HTTP_400_BAD_REQUEST)
#             user.set_password(form.cleaned_data['password2'])
#             user.save()
#             return Response(status=status.HTTP_202_ACCEPTED)
#         else:
#             return Response(status=status.HTTP_400_BAD_REQUEST)
#     else:
#         return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['POST', ])
def user_login(request):

    class LoginFormSerilizer(serializers.Serializer):
        email = serializers.EmailField()
        password = serializers.CharField(max_length=255)

    serializer = LoginFormSerilizer(request.data)
    form = forms.LoginForm(serializer.data)

    if form.is_valid():
        data = form.cleaned_data
        user = authenticate(username=data['email'], password=data['password'])

        if user is not None:
            if user.is_active:
                request.session['username'] = data['email']
                login(request, user)
                return Response(status=status.HTTP_200_OK)
            elif user.activation_key == 1:
                user.is_active = True
                user.activation_key += random.randint(2, 1000000)
                user.save()
                login(request, user)
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['GET', ])
def user_logout(request):
    logout(request)
    return Response(status=status.HTTP_200_OK)