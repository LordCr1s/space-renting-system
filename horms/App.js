import React from 'react';
import { Platform, StatusBar, StyleSheet, View, AsyncStorage, TouchableOpacity } from 'react-native';
import SplashScreen from './screens/intro/SplashScreen';
import TutorialScreen from './screens/intro/TutorialScreen';
import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
import Login from './screens/authentication/Login'
import Register from './screens/authentication/Register'
import Auth from './screens/authentication/Auth';
import Index from './screens/Index'
import { Provider } from 'react-redux'
import { horms_store } from './screens/Data/index'
import { Provider as PaperProvider } from 'react-native-paper'
import { theme } from './screens/authentication/styles'
import { Font } from 'expo'
import PlaceDetails from './screens/Home/HomeScreen/PlaceDetails'
import { setCustomText } from 'react-native-global-props'
import Renter from './screens/Home/Dashboard/Renter'

const AppStack = createStackNavigator({ Home: Index, Details: PlaceDetails, Renter: Renter, RootAuth: Auth, Register: Register, Login: Login });
const AuthStack = createStackNavigator({RootAuth: Auth, Register: Register, Login: Login });
const LoadingStack = createStackNavigator({TutorialScreen: TutorialScreen})

AppStack.navigationOptions = {
  header: null
}

Index.navigationOptions = {
  header: null
}

const Routing = createAppContainer(createSwitchNavigator(
  {
      App: AppStack,
      Auth: AuthStack,
      LoadingScreen: LoadingStack
  },
  {
      // initialRouteName: 'LoadingScreen'
      initialRouteName: 'App'
  }
))

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      isVisible: true,
      fontLoaded: false
    }
  }

  Hide_Splash_Screen=()=>{
    this.setState({
      isVisible : false
    });
  }

  async componentDidMount(){
    await Font.loadAsync({
      'ibm-bold': require('./assets/fonts/IBMPlexSans-Bold.ttf'),
      'noto-regular': require('./assets/fonts/NotoSans-Regular.ttf'),
      'noto-bold': require('./assets/fonts/NotoSans-Bold.ttf'),
    })

    var that = this;
    setTimeout(function(){
      that.Hide_Splash_Screen();
    }, 1000);

    this.setState({ fontLoaded: true });
    this.setDefaultText()
  }

  setDefaultText = () => {
    const customTextProps = {
      style: {
        fontFamily: 'noto-regular',
        color: 'rgba(0, 0, 0, .8)'
      }
    }
    setCustomText(customTextProps)
  }

  render() {

    let Splash_Screen = <SplashScreen />;

    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <Provider store={horms_store}>
            <PaperProvider theme={theme}>
              { this.state.fontLoaded ? <Routing style={{ fontFamily: 'ibm-regular' }}/> : null }
            </PaperProvider>
          </Provider>
        { (this.state.isVisible === true) ? Splash_Screen: null }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
