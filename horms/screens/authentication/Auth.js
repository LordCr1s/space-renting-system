import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, AsyncStorage} from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import API from './api';
import { isAuthenticated, userHasLogedOut } from '../authenticate'


export default class Auth extends Component {

    constructor(props) {
      super(props)
      this.state = {
        user: {},
        is_loggedin : false,
        is_fetching : true
      }
    }
    static navigationOptions = {
        headerStyle: {
            shadowOpacity: 0,
            shadowColor: 'transparent',
            elevation: 0,
            borderBottomWidth:0,
        },
      };


      logOut = async () => {
        const response = await API.makeGETrequest('logout')
        if (response.ok) {
            userHasLogedOut()
        } else {
        }
      }

      async componentDidMount() {
          this.checkAuth() 
      }

      checkAuth = async () => {
        const checker = await isAuthenticated()
        this.setState({
          is_loggedin: checker
        })
      }

  render() {


    return (

        this.state.is_loggedin ?

        <KeyboardAwareScrollView
            style={{ backgroundColor: '#FFFFFF' }}
            resetScrollToCoords={{ x: 0, y: 0 }}
            contentContainerStyle={styles.container}
            scrollEnabled={true}>

            <Text style={styles.title}> We're sorry to see you go </Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={() => this.logOut()} activeOpacity={0.7}>
                  <View style={[styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                      <AntDesign
                          name="login"
                          color="rgba(255, 255, 255, .9)"
                          size={18}
                          style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                      />
                    <Text style={styles.buttonText}>Log Out</Text>
                  </View>
              </TouchableOpacity>
                <Text style={styles.bottomText}> @2019 DIT, Inc. </Text>
            </View>
        </KeyboardAwareScrollView>

         :
        <KeyboardAwareScrollView
            style={{ backgroundColor: '#FFFFFF' }}
            resetScrollToCoords={{ x: 0, y: 0 }}
            contentContainerStyle={styles.container}
            scrollEnabled={true}>

            <Text style={styles.title}> Get started now </Text>
            <Image style={styles.image} source={require('../../assets/images/auth.jpg')} />
            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Login', {loginUser : this.loginUser})} activeOpacity={0.7}>
                    <View style={[styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                        <AntDesign
                            name="login"
                            color="rgba(255, 255, 255, .9)"
                            size={18}
                            style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                        />
                        <Text style={styles.buttonText}>Log In</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')} activeOpacity={0.7}>
                    <View style={[styles.button, {backgroundColor: '#00C851B3',}]}>
                        <AntDesign
                            name="adduser"
                            color="rgba(255, 255, 255, .9)"
                            size={18}
                            style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                        />
                        <Text style={styles.buttonText}>Sign Up</Text>
                    </View>
                </TouchableOpacity>
                <Text style={styles.bottomText}> @2019 DIT, Inc. </Text>
            </View>
        </KeyboardAwareScrollView>
    )
  }
}


const styles = StyleSheet.create({
    container : {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    image: {
        width: 400,
        height: 350,
        resizeMode: 'center',
    },
    title: {
        fontSize: 30,
        fontWeight: '300',
        paddingHorizontal: 16,
        color: 'rgba(0, 0, 0, .6)',
        position: 'absolute',
        top: 50,
    },
    buttonContainer : {
        flex : 1,
        flexDirection: 'column',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        alignItems: 'center',
    },
    button: {
        width: 300,
        paddingVertical: 13,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonText: {
        color: '#fff',
        marginLeft: 100
    },
    bottomText: {
        fontSize: 15,
        fontWeight: '200',
        color : 'rgba(0, 0, 0, .4)',
        margin: 30,
    }
});
