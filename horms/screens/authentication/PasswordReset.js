import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, TextInput} from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppNavigator from '../../navigation/AppNavigator';

export default class PasswordReset extends Component {

    // static navigationOptions = {
    //     headerStyle: {
    //         shadowOpacity: 0,
    //         shadowColor: 'transparent',
    //         elevation: 0,
    //         borderBottomWidth:0,
    //     },
    //   };

  render() {
    return ( 
        // <KeyboardAwareScrollView
        //     style={{ backgroundColor: '#FFFFFF' }}
        //     resetScrollToCoords={{ x: 0, y: 0 }}
        //     contentContainerStyle={styles.container}
        //     scrollEnabled={true}>

        //     <Text style={styles.title}> {"\tWelcome \n \tBack."} </Text>
        //     <Image style={styles.image} source={require('../../assets/images/auth.jpg')} />
            <View style={styles.buttonContainer}>
                <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10, fontSize: 20}}>
                    Provide your email
                </Text>
                <TextInput
                    style={styles.inputs}
                    placeholder="exmaple@shoo.com"
                />
                <TouchableOpacity onPress={this._onPress} activeOpacity={0.7}>
                    <View style={[styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                        <AntDesign
                            name="login"
                            color="rgba(255, 255, 255, .9)"
                            size={18}
                            style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                        />
                        <Text style={styles.buttonText}>Send reset code</Text>
                    </View> 
                </TouchableOpacity>
                <Text style={styles.bottomText}> @2019 Shoo, Inc. </Text>
            </View>
        // </KeyboardAwareScrollView>
    )
  }
}


const styles = StyleSheet.create({ 
    container : {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    image: {
        width: 400,
        height: 350,
        resizeMode: 'center',
        position: 'absolute',
        top: 100, 
    },
    title: { 
        fontSize: 50, 
        fontWeight: '300',
        paddingHorizontal: 16,
        color: 'rgba(0, 0, 0, .6)',
        position: 'absolute', 
        top: 50,
        left: 10, 
    },
    buttonContainer : {
        flex : 1,
        flexDirection: 'column',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        alignItems: 'center',
    },
    button: {
        width: 300,
        paddingVertical: 13,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
    },
    buttonText: {
        color: '#fff',
        marginLeft: 80 
    },
    bottomText: {
        fontSize: 15,
        fontWeight: '200',
        color : 'rgba(0, 0, 0, .4)',
        margin: 30,
    },
    inputs: {
        width: 300, 
        paddingVertical: 13,
        paddingHorizontal: 15,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
        backgroundColor: 'rgba(0, 0, 0, .1)',
    }
});