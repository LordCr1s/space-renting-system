import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, TextInput, AsyncStorage} from 'react-native'
import { AntDesign, MaterialCommunityIcons } from '@expo/vector-icons'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'
import { login_styles } from './styles'
import Toast from './toast';
import API from './api';
import Toaster, { ToastStyles } from 'react-native-toaster'
import { userHasLoggedIn } from '../authenticate'


class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user : {
                email: '',
                password: '',
            },
            toast: {
                show_error_toast: false,
                show_success_toast: false,
                toast_message: ''
            },
            verification_code: '',
            passwordResets: {
                password1: '',
                password2: ''
            }
        }
    }

    /* THE FOLLOWING THREE FUNCTIONS CONTROLS THE LOGIN SCREEN VIEW
     * DEPENDING ON THE ACTION A USER IS PERFORMING
     * */

    // show a screen to a let a user provide an email for receiving
    // password reset code, this function receives boolean value
    showPasswordResetEmail = (value) => {
        this.props.dispatch(this.props.actions.showLoginEmailReset(value))
    }

    // show a screen to verify the code sent to the user through their email
    // this function receives boolean value
    showResetCodeSection = (value, type='show') => {

        // after checking a code for password reset, this function gets called again
        // and we do not want it to do an api call, we just want it to hide this section
        // so i created a lookup table to choose what to do when calling this function
        const actions = {
            show: async (value) => {
                const response = await API.makePOSTrequest('resetmypassword', { email: this.state.user.email })
                if (response.ok) {
                    this.props.dispatch(this.props.actions.showResetCodeScreen(value))
                    // kill a previous screen
                    this.showPasswordResetEmail(false)
                } else {
                    Toast.printToast('error', {
                        value: true,
                        message: "Error!! the email you provided is invalid or not available!"
                    }, this)
                }
            },
            hide: (value) => {
                this.props.dispatch(this.props.actions.showResetCodeScreen(value))
                // kill a previous screen
                this.showPasswordResetEmail(false)
            }
        }

        actions[type](value)
    }

    // show a screen to let a user set new password
    // this function receives boolean value
    showPasswordUpdateSection = async (value) => {
        const url = 'activate/' + this.state.verification_code
        const response = await API.makeGETrequest(url)
        if (response.ok) {
            this.props.dispatch(this.props.actions.showPasswordUpdateScreen(value))
            // kill a previous screen (code verification screen)
            this.showResetCodeSection(false, 'hide')
        } else {
            Toast.printToast('error', {
                value: true,
                message: "Error!! the code you provided is invalid or expired!"
            }, this)
        }
    }

    static navigationOptions = {
        headerStyle: {
            shadowOpacity: 0,
            shadowColor: 'transparent',
            elevation: 0,
            borderBottomWidth:0,
            headerLeft: null
        },
      };

      updateUserInput = (type, value) => {
          let app_state = {
              ...this.state
          }

          const updateInput = {
              email: () => app_state.user.email = value,
              password: () => app_state.user.password = value,
              verification_code: () => app_state.verification_code = value
          }

          updateInput[type]()
          this.setState(app_state)
      }


      // store user object, this funtion will be moved to login screen
      // _storeData = async () => {
      //
      //   // user object
      //   const user = {'id': '2', 'email': 'chris@gmail.com', 'is_loggedin': 'true'}
      //   try {
      //       await AsyncStorage.setItem('user', JSON.stringify(user));
      //     } catch (error) {
      //       // Error saving data
      //     }
      //   };


      login = async () => {

        const response = await API.makePOSTrequest('login', this.state.user)
        if (response.ok) {
            userHasLoggedIn({email: this.state.user.email})
        } else {
            // this.props.navigation.navigate('App')
            Toast.printToast('error', {
                value: true,
                message: "Error!! invalid email or password"
            }, this)
        }
      }




  render() {



    // the view variable holds the default section to be seen at a login screen
    // it can be changed depending on user actions
    let view = <LoginSection
            showEmailReset = { this.showPasswordResetEmail }
            goToHomePage = { this.login}
            updateUserInput = { this.updateUserInput }/>

    if (this.props.viewControls.isPasswordResetemail) {
        // a section to a let a user provide an email for receiving password reset code
        view = <PasswordResetEmail
            showEmailReset = { this.showPasswordResetEmail }
            showResetCodeSection = { this.showResetCodeSection }
            updateUserInput = { this.updateUserInput }/>
    }

    if (this.props.viewControls.isPasswordResetCode) {
        // a section to let a user provide a code sent to their email
        view = <PasswordResetCode
            showResetCodeSection = { this.showResetCodeSection }
            showPasswordUpdateSection = { this.showPasswordUpdateSection }
            updateUserInput = { this.updateUserInput }/>
    }

    if (this.props.viewControls.isPasswordReset) {
        // a section to let a user update their new password
        view = <UpdatePassword showPasswordUpdateSection = { this.showPasswordUpdateSection } />
    }

    return (
        <KeyboardAwareScrollView
            style={{ backgroundColor: '#FFFFFF' }}
            resetScrollToCoords={{ x: 0, y: 0 }}
            contentContainerStyle={login_styles.container}
            scrollEnabled={true}>
            <Text style={login_styles.title}> {"\tWelcome \n \tBack."} </Text>
            { this.state.toast.show_error_toast ?
                <Toaster message = {
                    {
                        text: this.state.toast.toast_message,
                        styles: ToastStyles.error,
                        onHide: () => Toast.resetToast(this)
                    }
                }
                />:
                <Text> </Text>
            }

            { this.state.toast.show_success_toast ?
                < Toaster message = {
                    {
                        text: this.state.toast.toast_message,
                        styles: ToastStyles.success,
                        onHide: () => Toast.resetToast(this)
                    }
                }
                />:
                <Text> </Text>
            }
            <Image style={login_styles.image} source={require('../../assets/images/auth.jpg')} />
            { view }
        </KeyboardAwareScrollView>
    )
  }
}

const mapStateToProps = state => ({
    viewControls: state.login_provider.viewControl,
    actions: state.login_provider.actions,
})

export default connect(mapStateToProps)(Login)

// default section, a section for user to login
class LoginSection extends Component {

  render() {
    return (
        <View style={login_styles.buttonContainer}>
            <TextInput
                style={login_styles.inputs}
                placeholder="exmaple@email.com"
                keyboardType={'email-address'}
                onChangeText={(text) => this.props.updateUserInput('email', text)}
            />
            <TextInput
                style={login_styles.inputs}
                placeholder="password"
                secureTextEntry={true}
                onChangeText={(text) => this.props.updateUserInput('password', text)}
            />
            <TouchableOpacity onPress={() => this.props.goToHomePage()} activeOpacity={0.7}>
                <View style={[login_styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                    <AntDesign
                        name="login"
                        color="rgba(255, 255, 255, .9)"
                        size={18}
                        style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                    />
                    <Text style={login_styles.buttonText}>Log In</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.showEmailReset(true)} activeOpacity={0.7}>
                    <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10}}>Forgot password?</Text>
            </TouchableOpacity>
            <Text style={login_styles.bottomText}> @2019 DIT, Inc. </Text>
        </View>
    )
  }
}

// a section to a let a user provide an email for receiving password reset code
class PasswordResetEmail extends Component {
  render() {
    return (
        <View style={login_styles.buttonContainer}>
            <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10, fontSize: 20}}>
                Provide your email
            </Text>
            <TextInput
                style={login_styles.inputs}
                placeholder="exmaple@DIT.com"
                onChangeText={(text) => this.props.updateUserInput('email', text)}
            />
            <TouchableOpacity onPress={() => this.props.showResetCodeSection(true)} activeOpacity={0.7}>
                <View style={[login_styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                    <AntDesign
                        name="qrcode"
                        color="rgba(255, 255, 255, .9)"
                        size={18}
                        style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                    />
                    <Text style={[login_styles.buttonText, {marginLeft: 80}]}>Send reset code</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.showEmailReset(false)} activeOpacity={0.7}>
                    <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10}}>cancel?</Text>
            </TouchableOpacity>
            <Text style={login_styles.bottomText}> @2019 DIT, Inc. </Text>
        </View>
    )
  }
}

// a section to let a user provide a code sent to their email
class PasswordResetCode extends Component {
    render() {
      return (
          <View style={login_styles.buttonContainer}>
              <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10, fontSize: 20}}>
                  Provide code we sent to your email
              </Text>
              <TextInput
                  style={login_styles.inputs}
                  keyboardType={'numeric'}
                  onChangeText={(text) => this.props.updateUserInput('verification_code', text)}
              />
              <TouchableOpacity onPress={() => this.props.showPasswordUpdateSection(true)} activeOpacity={0.7}>
                  <View style={[login_styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                      <MaterialCommunityIcons
                          name="qrcode-scan"
                          color="rgba(255, 255, 255, .9)"
                          size={18}
                          style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                      />
                      <Text style={[login_styles.buttonText, {marginLeft: 80}]}>Verify code</Text>
                  </View>
              </TouchableOpacity>
              <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.showResetCodeSection(false)} activeOpacity={0.7}>
                            <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10, marginRight: 3}}>cancel?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => console.log('code resent!')} activeOpacity={0.7}>
                            <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10}}>resend code?</Text>
                    </TouchableOpacity>
              </View>
              <Text style={login_styles.bottomText}> @2019 DIT, Inc. </Text>
          </View>
      )
    }
  }

  // a section to let a user update their new password
class UpdatePassword extends Component {
    render() {
      return (
          <View style={login_styles.buttonContainer}>
              <TextInput
                  style={login_styles.inputs}
                  placeholder="new password"
                  secureTextEntry={true}
              />
              <TextInput
                  style={login_styles.inputs}
                  placeholder="repaeate new password"
                  secureTextEntry={true}
              />
              <TouchableOpacity onPress={() => console.log('update new password')} activeOpacity={0.7}>
                  <View style={[login_styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                      <MaterialCommunityIcons
                          name="key-variant"
                          color="rgba(255, 255, 255, .9)"
                          size={18}
                          style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                      />
                      <Text style={[login_styles.buttonText, {marginLeft: 80}]}>update password</Text>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.showPasswordUpdateSection(false)} activeOpacity={0.7}>
                    <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10}}>cancel?</Text>
              </TouchableOpacity>
              <Text style={login_styles.bottomText}> @2019 DIT, Inc. </Text>
          </View>
      )
    }
  }
