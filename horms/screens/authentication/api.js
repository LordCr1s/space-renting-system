const ROOT_API_URL = 'http://172.20.10.5:8006/api/'
// const ROOT_API_URL = 'http://192.168.137.145:8006/api/'

export default class API {

    static makePOSTrequest = (request_url, request_data) => {
        return fetch(ROOT_API_URL + request_url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request_data)
        }).then(response => response).catch(error => console.log(error))
    }

    static makeGETrequest = (request_url) => {
        return fetch(ROOT_API_URL + request_url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => response).catch(error => console.log(error))
    }
}
