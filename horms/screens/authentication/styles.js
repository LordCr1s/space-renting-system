import { StyleSheet } from 'react-native'
import { DefaultTheme } from 'react-native-paper'

export const login_styles = StyleSheet.create({ 
    container : {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    image: {
        width: 400,
        height: 350,
        resizeMode: 'center',
        position: 'absolute',
        top: 100, 
    },
    title: { 
        fontSize: 50, 
        fontWeight: '300',
        paddingHorizontal: 16,
        color: 'rgba(0, 0, 0, .6)',
        position: 'absolute', 
        top: 50,
        left: 10, 
    },
    buttonContainer : {
        flex : 1,
        flexDirection: 'column',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        alignItems: 'center',
    },
    button: {
        width: 300,
        paddingVertical: 13,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
    },
    buttonText: {
        color: '#fff',
        marginLeft: 100 
    },
    bottomText: {
        fontSize: 15,
        fontWeight: '200',
        color : 'rgba(0, 0, 0, .4)',
        margin: 30,
    },
    inputs: {
        width: 300, 
        paddingVertical: 13,
        paddingHorizontal: 15,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
        backgroundColor: 'rgba(0, 0, 0, .1)',
    }
});

export const registration_styles = StyleSheet.create({
    
    container : {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    image: {
        width: 400,
        height: 350,
        resizeMode: 'center',
        position: 'absolute',
        top: 100, 
    },
    title: { 
        fontSize: 50, 
        fontWeight: '300',
        paddingHorizontal: 16,
        color: 'rgba(0, 0, 0, .6)',
        position: 'absolute', 
        top: 50,
        left: 10, 
    },
    buttonContainer : {
        flex : 1,
        flexDirection: 'column',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        alignItems: 'center',
    },
    button: {
        width: 300,
        paddingVertical: 13,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
    },
    buttonText: {
        color: '#fff',
        marginLeft: 100 
    },
    bottomText: {
        fontSize: 15,
        fontWeight: '200',
        color : 'rgba(0, 0, 0, .4)',
        margin: 30,
    },
    inputs: {
        width: 300, 
        paddingVertical: 13,
        paddingHorizontal: 15,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
        backgroundColor: 'rgba(0, 0, 0, .1)',
    }
});

export const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#FFF',
      accent: 'rgba(0, 0, 0, .6)',
    },
  };