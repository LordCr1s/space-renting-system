import React, { Component } from 'react'
import { View, Text, Image, AsyncStorage, TouchableOpacity, TextInput} from 'react-native'
import { AntDesign, MaterialCommunityIcons } from '@expo/vector-icons'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { registration_styles, login_styles } from './styles'
import { connect } from 'react-redux'
import Toaster, { ToastStyles } from 'react-native-toaster'
import Toast from './toast'
import API from './api';

class Register extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user: {
                first_name: '',
                last_name: '',
                email: '',
                mobile_number: '',
                password1: '',
                password2: '',
            },
            toast: {
                show_error_toast: false,
                show_success_toast: false,
                toast_message: ''
            },
            app_stage: 'registration',
            verification_code: 0
        }
    }

    static navigationOptions = {
        headerStyle: {
            shadowOpacity: 0,
            shadowColor: 'transparent',
            elevation: 0,
            borderBottomWidth:0,
        },
      };


      // initialize registration stage when app starts
      componentDidMount() {
          AsyncStorage.getItem('app_stage').then(value => this.setState({
              ...this.state,
              app_stage: value
          }))
      }
      
      
    updateUserInput = (type, value) => {
        let app_state = {
            ...this.state
        }
        switch (type) {
            case 'first_name':
                app_state.user.first_name = value;
                break; 
            case 'last_name':
                app_state.user.last_name = value;
                break;
            case 'email':
                app_state.user.email = value;
                break;
            case 'mobile_number':
                app_state.user.mobile_number = value;
                break;
            case 'password1':
                app_state.user.password1 = value;
                break;
            case 'password2':
                app_state.user.password2 = value;
                break;
            default:
                break;
        }
        this.setState(app_state)
    }

    checkIfPasswordsMatch = () => {
        if (this.state.user.password1 == this.state.user.password2) {
            if (this.state.user.password1 != "" && this.state.user.password2 != "") {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }

    checkEmailValidity = () => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(this.state.user.email) == true) {
            return true
        } else {
            return false
        }
    }

    // validate user passwords and email before registering them to the system
    triggerRegistration = () => {
        if (this.checkEmailValidity()) {
            if (this.checkIfPasswordsMatch()) {
                // this.props.dispatch(this.props.actions.registerAuser(this.state.user))
                this.registerNewUser(this.state.user)
            } else {
                Toast.printToast('error', {
                    value: true,
                    message: "Error! Passwords do not match or not provided!!"
                }, this)
            }
        } else {
            Toast.printToast('error', {
                value: true,
                message: "Eror!! Email is invalid or not provided!!"
            }, this)
        }
    }
    
    
    registerNewUser = async user => {
        const response = await API.makePOSTrequest('register', user)
        if (response.ok) {
            // move to the account verification page and make sure the app remembers 
            // the stage even if the app is killed
            this.setState({
                ...this.state,
                app_stage: 'registration_code_verification'
            })
            AsyncStorage.setItem('app_stage', 'registration_code_verification')
        } else {
            Toast.printToast('error', {
                value: true,
                message: "Eror!! the email you provided already exist in the system"
            }, this)
        }
    }

    updateVerificationCodeinput = (value) => {
        this.setState({
            ...this.state,
            verification_code: value
        })
    }

    verifyRegistrationCode = async () => {
        const url = 'activate/' + this.state.verification_code
        const response = await API.makeGETrequest(url)
        if (response.ok) {
            AsyncStorage.setItem('app_stage', 'registration')
            this.setState({
                ...this.state,
                app_stage: 'registration'
            })
            Toast.printToast('success', {
                value: true,
                message: "Success!! account is successfully activated"
            }, this)
        } else {
            Toast.printToast('error', {
                value: true,
                message: "Eror!! Failed to activate account, code is invalid or expired"
            }, this)
        }
    }


  render() {
    return ( 
        <KeyboardAwareScrollView
            style={{ backgroundColor: '#FFFFFF' }}
            resetScrollToCoords={{ x: 0, y: 0 }}
            contentContainerStyle={registration_styles.container}
            scrollEnabled={true}>

            { this.state.toast.show_error_toast ? 
                <Toaster message = { 
                    {
                        text: this.state.toast.toast_message,
                        styles: ToastStyles.error,
                        onHide: () => Toast.resetToast(this)
                    }
                }
                />: 
                <Text> </Text> 
            }

            { this.state.toast.show_success_toast ? 
                < Toaster message = {
                    {
                        text: this.state.toast.toast_message,
                        styles: ToastStyles.success,
                        onHide: () => Toast.resetToast(this)
                    }
                }
                />: 
                <Text> </Text> 
            }
            
            <Text style={registration_styles.title}> {"\tCreate \n \tAccount."} </Text>
            { this.state.app_stage == 'registration_code_verification' ?
                <RegistrationVerificationCodeSection 
                    updateCodeInput={this.updateVerificationCodeinput}
                    verify={this.verifyRegistrationCode}
                    />:
                <View style={registration_styles.buttonContainer}>
                    <TextInput
                        style={registration_styles.inputs}
                        placeholder="first name"
                        onChangeText={text => this.updateUserInput('first_name', text)}
                    />
                    <TextInput
                        style={registration_styles.inputs}
                        placeholder="last name"
                        onChangeText={text => this.updateUserInput('last_name', text)}
                    />
                    <TextInput
                        style={registration_styles.inputs}
                        placeholder="exmaple@DIT.com"
                        onChangeText={text => this.updateUserInput('email', text)}
                    />
                    <TextInput
                        style={registration_styles.inputs}
                        placeholder="mobile number"
                        onChangeText={text => this.updateUserInput('mobile_number', text)}
                    />
                    <TextInput
                        style={registration_styles.inputs}
                        placeholder="password"  
                        secureTextEntry={true}
                        onChangeText={text => this.updateUserInput('password1', text)}
                    />
                    <TextInput
                        style={registration_styles.inputs}
                        placeholder="confirm password" 
                        secureTextEntry={true}
                        onChangeText={text => this.updateUserInput('password2', text)}
                    />
                    <TouchableOpacity onPress={this.triggerRegistration} activeOpacity={0.7}>
                    <View style={[registration_styles.button, {backgroundColor: '#00C851B3',}]}>
                            <AntDesign
                                name="adduser"
                                color="rgba(255, 255, 255, .9)"
                                size={18}
                                style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                            />
                            <Text style={registration_styles.buttonText}>Sign Up</Text>
                        </View>
                    </TouchableOpacity>
                    <Text style={registration_styles.bottomText}> @2019 Shoo, Inc. </Text>
                </View>}
        </KeyboardAwareScrollView>
    )
  }
}

class RegistrationVerificationCodeSection extends Component {
    render() {
      return (
          <View style={login_styles.buttonContainer}>
              <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10, fontSize: 20}}>
                  Provide code we sent to your email
              </Text>
              <TextInput
                  style={login_styles.inputs}
                  keyboardType={'numeric'}
                  onChangeText={text => this.props.updateCodeInput(text)}
              />
              <TouchableOpacity onPress={() => this.props.verify()} activeOpacity={0.7}>
                  <View style={[login_styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                      <MaterialCommunityIcons
                          name="qrcode-scan"
                          color="rgba(255, 255, 255, .9)"
                          size={18}
                          style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                      />
                      <Text 
                        style={[login_styles.buttonText, {marginLeft: 80}]}>Verify code</Text>
                  </View> 
              </TouchableOpacity>
              <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.showResetCodeSection(false)} activeOpacity={0.7}>
                            <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10, marginRight: 3}}>cancel?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => console.log('code resent!')} activeOpacity={0.7}>
                            <Text style={{color: 'rgba(0, 0, 0, .6)', marginTop: 10}}>resend code?</Text>
                    </TouchableOpacity>
              </View>
              <Text style={login_styles.bottomText}> @2019 Shoo, Inc. </Text>
          </View>
      )
    }
}

const mapStateToProps = state => ({
    actions: state.registration_provider.actions,
})

export default connect(mapStateToProps)(Register)