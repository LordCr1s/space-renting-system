import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet, AsyncStorage} from 'react-native';
import { Paragraph } from 'react-native-paper'
import { home_screen, theme } from './Styles'
import API from './api'
import {AntDesign, MaterialIcons} from '@expo/vector-icons';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import {
  Container,
  Header,
  Tab,
  Tabs,
  ScrollableTab,
  Button,
  Icon
} from "native-base";
import Requesting from './Dashboard/requesting'
import Contract from './Dashboard/contract'
import { isAuthenticated, getUserEmail } from '../authenticate'

export default class Dashboard extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
        requests: [],

        is_place_rented : false,
        is_loggedin: false,
        user: {}

    }
  }


  // getUserId = async () => {
  //   let user = '';
  //   try {
  //     user = await AsyncStorage.getItem('user') || 'none';
  //     if (user != 'none') {
  //       this.setState({
  //         user : JSON.parse(user),
  //         has_user_details: true
  //       })
  //     }
  //   } catch (error) {
  //     // Error retrieving data
  //     console.log(error.message);
  //   }
  // }

  async componentDidMount() {
    await this.checkAuth()
    const user = await getUserEmail()

    this.setState({
      user : user
    })
  }


  static navigationOptions = {
    header: null
  };

  goToUserDetails = (item, _user) => {
    this.props.navigation.navigate('Renter', {request : item, user: _user})
  }

  checkAuth = async () => {
    const checker = await isAuthenticated()
    this.setState({
      is_loggedin: checker
    })
  }


  render() {
    return (
      this.state.is_loggedin ?
      <View style={[home_screen.container, {}]}>
        <React.Fragment>
          <View style={ [home_screen.search_bar_container, {height: heightPercentageToDP('.5%')}] }>
            <MaterialIcons
              name="bookmark"
              size={24}
              color={'#FFF'}/>
            <Text style={styles.title}>Dashboard</Text>
          </View>
          <View style={home_screen.contect_view}>
            <Container style={styles.container}>
              <Tabs
                style={styles.tab_bar_style}
                tabBarUnderlineStyle={styles.tab_underline_style}>
                <Tab
                  heading="REQUESTING"
                  tabStyle={styles.tab}
                  activeTabStyle={styles.tab}
                  textStyle={styles.tab_text}
                  activeTextStyle={styles.tab_text}>
                  <Requesting user={this.state.user} />
                </Tab>
                <Tab
                  heading="IN CONTRACT"
                  tabStyle={styles.tab}
                  activeTabStyle={styles.tab}
                  textStyle={styles.tab_text}
                  activeTextStyle={styles.tab_text}>
                  <Contract goToUserDetails={this.goToUserDetails}/>
                </Tab>
              </Tabs>
            </Container>
          </View>
        </React.Fragment>

      </View>
      :

      <Text style={{ position: 'absolute', marginTop: heightPercentageToDP('50%'), width: widthPercentageToDP('100%'), textAlign: 'center'}}>
          please login to see a Dashboard
      </Text>


    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#FFF',
    fontFamily: 'noto-regular',
    fontSize: heightPercentageToDP('2.5%'),
    width: widthPercentageToDP('90%'),
    textAlign: 'center',
  },
  heading: {
    padding: 20,
    paddingBottom: 0,
    color: "rgba(0, 0, 0, 0.7)"
  },
  subtitle: {
    color: "rgba(0, 0, 0, 0.4)",
    paddingLeft: 20,
    paddingBottom: 10
  },
  description: {
    paddingTop: 0,
    paddingBottom: 3
  },
  short_info: {
    flexDirection: "row",
    alignItems: "center",
    justifyItems: "space-around"
  },
  short_info_content: {
    flexDirection: "column",
    justifyItems: "space-around"
  },
  tab_bar_style: {
    width: widthPercentageToDP("100%"),
  },
  tab: {
    backgroundColor: "rgb(0, 152, 95)",
    height: heightPercentageToDP('1%')

  },
  tab_text: {
    color: 'white'
  },
  details: {
    padding: 5,
    paddingTop: 10,
    color: "rgba(0, 0, 0, 0.7)"
  },
  container: {
    height: heightPercentageToDP(".5%")
  },
  tab_underline_style: {
    borderRadius: 10,
    backgroundColor: '#FFF'
  },
  snackbar: {
    width: widthPercentageToDP("70%"),
    marginLeft: widthPercentageToDP("15%"),
    bottom: widthPercentageToDP("5%")
  },
  surface: {
    padding: 8,
    height: heightPercentageToDP('5%'),
    width: widthPercentageToDP('30%'),
    marginLeft: widthPercentageToDP('35%'),
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
    color: '#0cc2aa'
  },
  appbar : {
    height: heightPercentageToDP('5%')
  },
});
