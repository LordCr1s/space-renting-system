import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet} from 'react-native';
import { Searchbar, Avatar, Button, Card, Title, Paragraph } from 'react-native-paper'
import { home_screen, theme } from './Styles'
import API from './api'
import Place from './HomeScreen/Place'
import AntDesign from '@expo/vector-icons/AntDesign';
import { heightPercentageToDP } from 'react-native-responsive-screen';

export default class HomeScreen extends React.Component {

  static navigationOptions = {
    header: null,
  };

  _keyExtractor = (item) => item.id;

  constructor(props) {
    super(props)
    this.state = {
      places: [],
      firstQuery: '',
      is_searching: false,
      search_results: [],
    }
  }


  async componentDidMount() {
    const places = await API.makeGETrequest('places')
    this.setState({
      ...this.state,
      places: places,
    })
  }


  showPlaceDetails = (item) => {
    this.props.navigation.navigate('Details', {place : item, type: 'other'})
  }

  search = (query) => {
    let places = [...this.state.places]
    const new_places = places.filter(place => {
      if (place.title.toLowerCase().includes(query.toLowerCase()))
          return place
  })
    this.setState({ firstQuery: query, search_results: new_places, is_searching: true });
  }


  render() {

    const { firstQuery } = this.state;

    return (
      <View style={home_screen.container}>
        <View style={ home_screen.search_bar_container }>
          <AntDesign
            name='home'
            size={24}
            color='#FFF'
            style={{ top: heightPercentageToDP('.6%')}}/>
          <Searchbar
            placeholder="search places"
            style={ home_screen.search_bar }
            theme={ { colors: {primary: '#FFF', accent: '#FFFFFF', text: '#FFFFFF', placeholder: '#FFFFFF'}} }
            onChangeText={query => this.search(query)}
            value={firstQuery}
            />
        </View>
        <View style={home_screen.contect_view}>
          {
            this.state.is_searching ?
            <FlatList
              keyExtractor={this._keyExtractor}
              data={this.state.search_results}
              renderItem={({item}) =>
              <TouchableOpacity
                onPress={() => this.showPlaceDetails(item)}
                activeOpacity={0.7}>
                <Place _place={item}/>
              </TouchableOpacity>
            }/>
            :
            <FlatList
              keyExtractor={this._keyExtractor}
              data={this.state.places}
              renderItem={({item}) =>
              <TouchableOpacity
                onPress={() => this.showPlaceDetails(item)}
                activeOpacity={0.7}>
                <Place _place={item}/>
              </TouchableOpacity>
            }/>
          }
        </View>
      </View>
    );
  }
}
