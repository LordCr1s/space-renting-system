import React, { Component } from 'react'
import { View, ListView, Button, TextInput } from '@shoutem/ui';
import { StyleSheet, TouchableOpacity, Text, KeyboardAvoidingView } from 'react-native'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { AntDesign, MaterialIcons } from '@expo/vector-icons'
import { Avatar } from 'react-native-paper'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import API, { image_root_url } from '../api'


export default class Renter extends Component {

  // modifying the stack component view
  static navigationOptions = {

    // removing default header for the custom header
    header: null
  };

  constructor(props) {
    super(props)
    this.state = {
      payments: [],
        new_payment : {
          contract_id: "",
          amount: "",
          duration: ""
        },
        // snackbar variables
        snackbar_visible : false,
        snackbar_message : 'default text',
    }
  }

  cancelRequest = async (payment) => {

    const response = await API.makePOSTrequest('modifypayment', {
      payment_id: payment.id
    })

    if (response.ok) {
      let payments = [...this.state.payments]
      let index = payments.indexOf(payment)
      payments[index].status = 'cancelled'

      this.setState({
        ...this.state,
        payments: payments
      })
    }


  }

  addPayment = async (_contract_id) => {

    const response = await API.makePOSTrequest('addpayment', {
      contract_id: _contract_id,
      amount: this.state.new_payment.amount,
      duration: this.state.new_payment.duration
    })

    if (response.ok) {
      const payment = await response.json()

      let payments = [...this.state.payments]
      payments.push(payment)

      this.setState({
        payments: payments
      })

    }
  }

  updateUserInput = (type, value=null) => {
    let new_payment = {...this.state.new_payment}

    const updateInput = {
        amount: () => new_payment.amount = value,
        duration: () => new_payment.duration = value,
    }

    updateInput[type]()
    this.setState({new_payment: new_payment})
  }

  componentDidMount() {

    // the request this page is supposed to show details for
    const request = this.props.navigation.getParam('request')
    const user = this.props.navigation.getParam('user')

    this.setState({
      payments: request.payments,
      contract: request,
      user: user
    })
  }


  // render payment info row
  renderRow = payment => {
    return (
      <View style={styles.payment}>
          {
            (this.state.contract.owner.email == this.state.user.email)?

            (payment.status == 'cancelled')?
              <React.Fragment>
                <Text style={[styles.normal_text, {flex: 1, backgroundColor: 'rgb(0,0,0,0.5)', textDecorationLine: 'line-through'}]}> { payment.id } </Text>
                <Text style={[styles.normal_text, {flex: 4, textDecorationLine: 'line-through'}]}> { payment.amount } </Text>
                <Text style={[styles.normal_text, {flex: 5, textDecorationLine: 'line-through'}]}> { payment.payed_at } </Text>
                <Text style={[styles.normal_text, {flex: 3, textDecorationLine: 'line-through'}]}> { payment.duration } </Text>
                <Text style={{ color: 'rgb(229, 94, 91)'}}>cancelled</Text>
              </React.Fragment>
            :
            <React.Fragment>
              <Text style={[styles.normal_text, {flex: 1, backgroundColor: 'rgb(0,0,0,0.5)'}]}> { payment.id } </Text>
              <Text style={[styles.normal_text, {flex: 4}]}> { payment.amount } </Text>
              <Text style={[styles.normal_text, {flex: 5}]}> { payment.payed_at } </Text>
              <Text style={[styles.normal_text, {flex: 3}]}> { payment.duration } </Text>
              <Button styleName="secondary" style={styles.cancel_button} onPress={() => this.cancelRequest(payment)}>
                <Text style={styles.white_text}>cancel</Text>
              </Button>
            </React.Fragment>

            :
            (payment.status == 'cancelled')?
              <React.Fragment>
                <Text style={[styles.normal_text, {flex: 1, backgroundColor: 'rgb(0,0,0,0.5)', textDecorationLine: 'line-through'}]}> { payment.id } </Text>
                <Text style={[styles.normal_text, {flex: 4, textDecorationLine: 'line-through'}]}> { payment.amount } </Text>
                <Text style={[styles.normal_text, {flex: 5, textDecorationLine: 'line-through'}]}> { payment.payed_at } </Text>
                <Text style={[styles.normal_text, {flex: 3, textDecorationLine: 'line-through'}]}> { payment.duration } </Text>
                <Text style={{ color: 'rgb(229, 94, 91)'}}>cancelled</Text>
              </React.Fragment>
              :
              <React.Fragment>
                <Text style={[styles.normal_text, {flex: 1, backgroundColor: 'rgb(0,0,0,0.5)'}]}> { payment.id } </Text>
                <Text style={[styles.normal_text, {flex: 4}]}> { payment.amount } </Text>
                <Text style={[styles.normal_text, {flex: 5}]}> { payment.payed_at } </Text>
                <Text style={[styles.normal_text, {flex: 3}]}> { payment.duration } </Text>
                <Text style={{ color: 'rgb(0, 152, 95)'}}>active pay</Text>
              </React.Fragment>
          }

      </View>
    )
  }


  render() {

    // the request this page is supposed to show details for
    const request = this.props.navigation.getParam('request')
    const user = this.props.navigation.getParam('user')

    return (

        <View style={styles.main_container}>

          {/* navigation bar */}
          <View style={styles.navbar_container}>

            {/* back button */}
            <TouchableOpacity
              style={{ marginLeft: 10}}
              onPress={() => this.props.navigation.goBack()}>
              <AntDesign
                name="arrowleft"
                size={20}
                color={'#FFF'} />
            </TouchableOpacity>

            {/* navbar header */}
            <Text style={styles.navbar_header}>
              {request.renter.first_name + ' ' + request.renter.last_name}
            </Text>

          </View>

          {/* content view */}
          <View style={styles.contect_view}>


            {/* user details section */}
            <View style={styles.top_view}>

              {/* renter profile picture */}
              <Avatar.Image
                size={100}
                source={{ uri: image_root_url + request.renter.userprofile.profile_picture }}
                style={{ marginTop: 0}}/>

              {/* renter's and renting information */}
              <View style={styles.renting_info}>
                <Text style={styles.bold_text}>
                  {request.renter.first_name + ' ' + request.renter.last_name}
                </Text>
                <Text style={styles.normal_text}>
                  {request.place.title}
                </Text>
                <Text style={styles.normal_text}>
                  <MaterialIcons
                    name="access-time"
                    size={12}
                    color={'rgba(0, 0, 0, 0.6)'} /> rented on {request.created_at}
                  </Text>
                  <Text style={styles.normal_text}>
                    <MaterialIcons
                      name="location-on"
                      size={15}
                      color={'rgba(0, 0, 0, 0.6)'} /> located at {request.place.location}
                    </Text>
                  </View>

                </View>

                {/* payment information */}

                <View style={styles.payment_information_container}>
                  <ListView
                    data={this.state.payments}
                    renderRow={this.renderRow}
                    />
                </View>
                {
                  (request.owner.email == user.email)?
                  <KeyboardAvoidingView
                     style={styles.payment}
                     behavior="position"
                     contentContainerStyle={styles.payment_keyboard}>

                    <TextInput
                      placeholder={'amount'}
                      style={{flex: 3,}}
                      keyboardType={'numeric'}
                      onChangeText={(text) => this.updateUserInput('amount', text)}
                      />
                    <TextInput
                      placeholder={'duration'}
                      onChangeText={(text) => this.updateUserInput('duration', text)}
                      style={{flex: 3}}
                      />
                    <Button
                      styleName="secondary"
                      style={styles.add_button}
                      onPress={() => this.addPayment(request.id)}>
                      <Text style={styles.white_text}>add</Text>
                    </Button>
                  </KeyboardAvoidingView> : null
                }


              </View>

            </View>

    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(0, 152, 95)',
  },

  // navigation bar styles
  navbar_container: {
    backgroundColor: 'rgb(0, 152, 95)',
    height: heightPercentageToDP('6%'),
    justifyContent: 'center',
    top: heightPercentageToDP('1.4%'),
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  navbar_header: {
    fontFamily: 'noto-bold',
    color: '#FFF',
    width: widthPercentageToDP('90%'),
    textAlign: 'center',
  },

  // content view styles
  contect_view: {
    flex: 10,
    backgroundColor: 'rgb(249, 249, 250)',
    width: widthPercentageToDP('100%'),
    alignItems: 'center',
  },
  contect_view_keyboard: {
    flex: 1,
    backgroundColor: 'rgb(249, 249, 250)',
    width: widthPercentageToDP('100%'),
    alignItems: 'center',
    height: heightPercentageToDP("20%")
  },

  // top view styles
  top_view: {
    backgroundColor: '#FFF',
    width: widthPercentageToDP('100%'),
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  renting_info: {
    width: widthPercentageToDP('65%'),
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 20
  },
  bold_text: {
    fontFamily: 'noto-bold',
    fontSize: heightPercentageToDP('2.5%'),
    marginBottom: 5
  },
  normal_text: {
    fontSize: heightPercentageToDP('2%'),
    marginBottom: 2,
    marginLeft: 5
  },
  payment_information_container: {
    justifyContent: 'flex-start',
    backgroundColor: '#FFF',
    marginTop: 5,
    width: widthPercentageToDP('100%'),
    height: heightPercentageToDP('60%')
  },
  payment: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 1,
  },
  payment_keyboard: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: heightPercentageToDP('8%')
  },
  cancel_button: {
    flex: 2,
    backgroundColor: '#f5e071',
    borderColor: '#f5e071',
    height: heightPercentageToDP('3%'),
  },
  add_button: {
    flex: 1,
    backgroundColor: 'rgb(0, 152, 95)',
    borderColor: 'rgb(0, 152, 95)',
    height: heightPercentageToDP('3%'),
    marginTop: heightPercentageToDP('2%')
  },
  white_text: {
    color: 'white',
    textAlign: 'center'
  },
})
