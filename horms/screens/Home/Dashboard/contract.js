import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet, AsyncStorage, ActivityIndicator} from 'react-native';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { ListView, Button as Shot_button } from '@shoutem/ui'
import { Card, Avatar } from 'react-native-paper'
import { AntDesign } from '@expo/vector-icons'
import API, { image_root_url } from '../api'


export default class Contract extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props)
    this.state = {
        requests: [],
        user: {},
        has_user_details : false,
        is_fetching : true
    }
  }

  showUserDetails = (item, user) => {
    this.props.goToUserDetails(item, this.state.user)
  }


  async componentDidMount() {
    this.getContracts()
  }

  // get all contracts
  getContracts = async () => {
    const requests = await API.makeGETrequest('getcontracts/2')
    this.setState({
      ...this.state,
      requests: requests,
      is_fetching: false
      })
  }

  renderRow = request => {
      return (
        <TouchableOpacity
          onPress={() => this.showUserDetails(request)}
          activeOpacity={0.7}>
          <View style={styles.request}>
            <Avatar.Image
              size={34}
              source={{ uri: image_root_url + request.renter.userprofile.profile_picture }}
              style={{ marginTop: 0}}/>
            <View style={styles.request_content}>
              <Text style={styles.request_header}>
                {request.renter.first_name + ' ' + request.renter.last_name}
              </Text>
              <Text style={styles.request_place_name}>
                {request.place.title}
              </Text>
              <Text style={styles.request_date}>
                {request.created_at}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      )
  }

  render() {
    return (
      <View>
        {
          this.state.is_fetching ?
          <View style={{marginTop: heightPercentageToDP('35%')}}>
              <ActivityIndicator size="small" color="rgba(0,0,0,0.7)" />
          </View>
          :
          <ListView
              data={this.state.requests}
              renderRow={this.renderRow}
          />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
    request : {
        flexDirection: 'row',
        backgroundColor: '#FFF',
        padding: 8,
        marginTop: 1,
        alignItems: 'center'
    },
    request_content: {
        flexDirection: 'column',
        marginLeft: widthPercentageToDP('3%'),
    },
    request_header: {
        color: 'rgba(0, 0, 0, 0.8)',
        fontFamily: 'noto-bold'
    },
    request_date : {
        color: 'rgba(0, 0, 0, 0.4)',
        fontSize: heightPercentageToDP('1.4%')
    },
    request_place_name: {

    },
    accept_button: {
        color: 'white',
        height: heightPercentageToDP('3%'),
        backgroundColor: 'rgb(0, 152, 95)',
        borderColor: 'rgb(0, 152, 95)',
    },
    reject_button: {
        color: 'white',
        height: heightPercentageToDP('3%'),
        backgroundColor: 'rgb(229, 94, 91)',
        borderColor: 'rgb(229, 94, 91)',
        marginLeft: widthPercentageToDP('.8%')
    },
    white_text: {
        color: 'white',
        textAlign: 'center'
    }
});
