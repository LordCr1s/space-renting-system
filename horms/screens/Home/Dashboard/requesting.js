import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet, AsyncStorage, ActivityIndicator} from 'react-native';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { ListView, Button } from '@shoutem/ui'
import { Snackbar, Avatar, DefaultTheme } from 'react-native-paper'
import { AntDesign } from '@expo/vector-icons'
import API, { image_root_url } from '../api'
import { getUserEmail } from '../../authenticate'


const avatar = require('../../../assets/images/avatar.jpg')
const default_avatar = <Avatar.Image size={45} source={avatar} style={{ marginTop: 0}}/>


export default class Requesting extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props)
    this.state = {
        requests: [],

        // request content
        request_status: false,
        request_text: 'accepted',


        // snackbar variables
        snackbar_visible : false,
        snackbar_message : 'default text',
        is_fetching : true,

        user : {}
    }
  }

  async componentDidMount() {
    let user = await getUserEmail()
    
    const requests = await API.makeGETrequest('requests/' + user.email)
    this.setState({
      ...this.state,
      requests: requests,
      is_fetching: false,
      user : user
      })
  }


  // reply to a request
  replyRequest = async (request_reply, id) => {
    const response = await API.makePOSTrequest('modifyrequest', {
      reply: request_reply,
      request_id: id
    })

    if (response.ok){
      // new requests list
      let request_list = this.state.requests.filter(request => request.id != id)

      this.setState({
          requests : request_list,
          snackbar_visible: true,
          snackbar_message: request_reply + " successfully!!",
      })
    }
  }


  renderRow = request => {
      return (
        <View style={styles.request}>
          <Avatar.Image
            size={34}
            source={{ uri: image_root_url + request.sender.userprofile.profile_picture }}
            style={{ marginTop: 0}}/>
          <View style={styles.request_content}>
            <Text style={styles.request_header}>
              {request.sender.first_name + " " + request.sender.last_name}
            </Text>
            <Text style={styles.request_place_name}>
              {request.place.title}
            </Text>
            <Text style={styles.request_date}>
              {request.sent_at}
            </Text>
          </View>
          <View style={styles.button_container}>
            {
              (this.state.user.email == request.sender.email) ?
                  <Button
                    styleName="secondary"
                    style={styles.cancel_button}
                    onPress={() => this.replyRequest('cancelled', request.id)}>
                    <Text style={styles.white_text}>cancel</Text>
                  </Button>
                 :
                <React.Fragment>
                  <Button
                    styleName="secondary"
                    style={styles.accept_button}
                    onPress={() => this.replyRequest('accepted', request.id)}>
                    <Text style={styles.white_text}>accept</Text>
                  </Button>
                  <Button
                    styleName="secondary"
                    style={styles.reject_button}
                    onPress={() => this.replyRequest('rejected', request.id)}>
                    <Text style={styles.white_text}>reject</Text>
                  </Button>
                </React.Fragment>
            }
          </View>
        </View>
      )
  }

  render() {

    return (
        <View>
          {
            this.state.is_fetching ?
            <View style={{marginTop: heightPercentageToDP('35%')}}>
                <ActivityIndicator size="small" color="rgba(0,0,0,0.7)" />
            </View>
            :
            <ListView
                data={this.state.requests}
                renderRow={this.renderRow}
            />
          }
          <Snackbar
            visible={this.state.snackbar_visible}
            onDismiss={() => this.setState({ snackbar_visible: false })}
            style={styles.snackbar}
            theme = {_theme}
            duration = {2500}>
            { this.state.snackbar_message }
          </Snackbar>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    request : {
        flexDirection: 'row',
        backgroundColor: '#FFF',
        padding: 8,
        marginTop: 1,
        alignItems: 'center'
    },
    request_content: {
        flexDirection: 'column',
        marginLeft: widthPercentageToDP('3%'),
        flex: 2
    },
    request_header: {
        color: 'rgba(0, 0, 0, 0.8)',
        fontFamily: 'noto-bold'
    },
    request_date : {
        color: 'rgba(0, 0, 0, 0.4)',
        fontSize: heightPercentageToDP('1.4%')
    },
    request_place_name: {

    },
    accept_button: {
        color: 'white',
        height: heightPercentageToDP('3%'),
        backgroundColor: 'rgb(0, 152, 95)',
        borderColor: 'rgb(0, 152, 95)',
    },
    cancel_button: {
        color: 'white',
        height: heightPercentageToDP('3%'),
        backgroundColor: 'rgb(255, 193, 7)',
        borderColor: 'rgb(255, 193, 7)',
        marginLeft: widthPercentageToDP('13%')
    },
    reject_button: {
        color: 'white',
        height: heightPercentageToDP('3%'),
        backgroundColor: 'rgb(229, 94, 91)',
        borderColor: 'rgb(229, 94, 91)',
        marginLeft: widthPercentageToDP('.8%')
    },
    white_text: {
        color: 'white',
        textAlign: 'center'
    },
    button_container : {
        flexDirection: 'row',
        right: 0,
        flex: 1
    },
    snackbar: {
        position: 'absolute',
        width: widthPercentageToDP("70%"),
        marginLeft: widthPercentageToDP("15%"),
        top: heightPercentageToDP("2%")
      },
});

export const _theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'rgba(0, 0, 0, .6)',
      accent: '#FFF',
    },
  };
