import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet, AsyncStorage} from 'react-native';
import { Appbar } from 'react-native-paper'
import { home_screen, theme } from './Styles'
import API from './api'
import Place from './HomeScreen/Place'
import { NavigationBar, Title, Button } from '@shoutem/ui'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { AntDesign } from '@expo/vector-icons'
import {Header,} from "native-base";
import { isAuthenticated } from '../authenticate'

export default class HomeScreen extends React.Component {

  static navigationOptions = {
    header: null,
  };

  _keyExtractor = (item) => item.id;

  constructor(props) {
    super(props)
    this.state = {
      places: [],
      is_fetching : true,
      is_loggedin: false
    }
  }


  getPlaces = async () => {
    const places = await API.makeGETrequest('places/2')
    this.setState({
      ...this.state,
      places: places,
    })
  }

  showPlaceDetails = (item) => {
    this.props.navigation.navigate('Details', {place : item, type: 'mine'})
  }


  async componentDidMount() {
    await this.checkAuth()
    if (this.state.is_loggedin) {
      this.getPlaces()
    }
  }



  checkAuth = async () => {
    const checker = await isAuthenticated()
    this.setState({
      is_loggedin: checker
    })
  }


  render() {
    return (
      this.state.is_loggedin ?
      <View style={home_screen.contect_view}>
         <NavigationBar
            styleName="inline"
            centerComponent={
              <Title style={{ fontWeight: 'bold'}}>
                My Posts
              </Title>
            }
            rightComponent={
              <Button style={{marginRight: widthPercentageToDP('3%')}}>
                <AntDesign name="pluscircle" size={24} />
              </Button>
            }
          />

        <FlatList style={{ paddingTop: heightPercentageToDP('1.5%')}} keyExtractor={this._keyExtractor} data={this.state.places} renderItem={({item}) =>
              <TouchableOpacity onPress={() => this.showPlaceDetails(item)} activeOpacity={0.7}>
                <Place _place={item}/>
              </TouchableOpacity>
            }/>
        </View> :

        <Text style={{ position: 'absolute', marginTop: heightPercentageToDP('50%'), width: widthPercentageToDP('100%'), textAlign: 'center'}}>
            please login to see a Dashboard
        </Text>
    );
  }
}

const styles = StyleSheet.create({
  appbar : {
    height: heightPercentageToDP('5%')
  },
});
