import React, { Component } from "react";
import {
  View,
  FlatList,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView
} from "react-native";
import { home_screen, theme } from "../Styles";
import {
  NavigationBar,
  Title,
  Image,
  Heading,
  Subtitle,
  Button as ShotemButton,
  InlineGallery
} from "@shoutem/ui";
import { AntDesign } from "@expo/vector-icons";
import {
  Card,
  Paragraph,
  Dialog,
  Portal,
  Button as PaperButton,
  Snackbar,
  DefaultTheme,
  Surface,
  Appbar,
} from "react-native-paper";
import API from "../api";
import {
  Container,
  Header,
  Tab,
  Tabs,
  ScrollableTab,
  Button,
  Icon
} from "native-base";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "react-native-responsive-screen";
import { DataTable } from 'react-native-paper';
import { image_root_url } from '../api'
import {AsyncStorage} from 'react-native';
import { MapView, Marker } from 'expo'
import { isAuthenticated } from '../../authenticate'




export default class PlaceDetails extends Component {

    static navigationOptions = {
      header: null
    };

    constructor(props) {
      super(props);
      this.state = {
        place_info: {},
        visible: false,

        // snackbar variables
        snackbar_visible : false,
        snackbar_message : 'default text',

        // update this part of a state after connection with the database
        is_place_rented : false,

        is_loggedin: false,

        // images
        images : [],
      };
    }


    async componentDidMount() {
      this.checkAuth()
    }

    checkAuth = async () => {
      const checker = await isAuthenticated()
      this.setState({
        is_loggedin: checker
      })
    }


   _showDialog = () => this.setState({ visible: true });

  _hideDialog = () => this.setState({ visible: false });



  // handle rent a place button actions
  rentAplace = async (rent, id) => {
    // make api request here
    if (this.state.is_loggedin) {
      // make api call
      const response = await API.makePOSTrequest('rentplace', {
        user_email: this.state.user_email,
        place_id: id
      })
      if (response.ok) {
        this.setState({
          snackbar_visible: true,
          snackbar_message: "action successfull",
          is_place_rented: rent
        });
      } else {
        this.setState({
          snackbar_visible: true,
          snackbar_message: "action unsuccessfull",
          is_place_rented: rent
        });
      }

    } else {
      // indicate that action has been done with a snackbar
      this.setState({
        snackbar_visible: true,
        snackbar_message: "login to rent a place",
        is_place_rented: rent
      });
    }

  }

  // handle delete a place action
  deleteAplace = () => {
    // make api request here

    // indicate that action has been done with a snackbar
    this.setState({
      snackbar_visible: true,
      snackbar_message: "deleted successfully!!",
    });
  }

  createImageSeries = db_images => {
    let images = []
    db_images.forEach(image => {
      images.push({"source": {"uri": image_root_url + image.name }})
    });
    return db_images
  }



  render() {
    
    let place_type = {
      1: () => {return 'House'},
      2: () => {return 'Appartment'},
      3: () => {return 'Office'},
      4: () => {return 'Hall'}
    }

    let place_status = {
      1: () => {return 'New construction'},
      2: () => {return 'Good condition'},
      3: () => {return 'Old construction'},
    }

    // snackbar visibility controll
    const { snackbar_visible } = this.state

    const place = this.props.navigation.getParam('place')
    const type = this.props.navigation.getParam('type')
    const place_info = this.state.place_info

    // creating image list
    let images = []

    // place.place_images.forEach(image => {
    //   images.push({"source": {"uri": image_root_url + image.image }})
    // });

    return (

      <ScrollView style={home_screen.contect_view}>
        <NavigationBar
          centerComponent={<Title>{ place_type[place.place_type]()  }</Title>}
          leftComponent={(
            <TouchableOpacity
              style={{ marginLeft: 10}}
              onPress={() => this.props.navigation.goBack()}>
              <AntDesign name="arrowleft" size={20} />
            </TouchableOpacity>
          )}
        />
        <View style={home_screen.place_details_container}>
        <InlineGallery
          styleName="large-square"
          data={images}
        />
          <Heading style={styles.heading}>{place.title}</Heading>
          <Subtitle style={styles.subtitle}>{ place.user.first_name + " " + place.user.last_name }</Subtitle>
          <View style={styles.short_info }>
            <View style={styles.short_info_content}>
              <Paragraph style={[styles.heading, styles.description]}>
                Price
              </Paragraph>
              <Paragraph style={[styles.heading, styles.description]}>
                <Title style={{ fontWeight: 'bold'}}>
                  {
                    ((place.place_type == 1)? place.living_place.price:
                    (place.place_type == 3)? place.office.price:
                    (place.place_type == 4)? place.hall.price: null)
                      + " " +
                    ((place.place_type == 1)? place.living_place.payment_terms:
                    (place.place_type == 3)? place.office.payment_terms:
                    (place.place_type == 4)? place.hall.payment_terms: null)
                  }
                </Title>
              </Paragraph>
            </View>
            <View style={styles.short_info_content}>
              <Paragraph style={[styles.heading, styles.description]}>
                Condition
              </Paragraph>
              <Paragraph style={[styles.heading, styles.description]}>
                <Title>{ place_status[place.status]() }</Title>
              </Paragraph>
            </View>
          </View>

                <Paragraph style={styles.details}>
                   { (place.place_type == 1)? place.living_place.descritpion:
                      (place.place_type == 3)? place.office.descritpion:
                      (place.place_type == 4)? place.hall.descritpion: null
                    }
                </Paragraph>

                { (place.place_type == 1)?
                      <DataTable>
                        <DataTable.Row>
                          <DataTable.Cell>rooms</DataTable.Cell>
                          <DataTable.Cell>{place.living_place.rooms}</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                          <DataTable.Cell>master rooms</DataTable.Cell>
                          <DataTable.Cell>{place.living_place.master_rooms}</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                          <DataTable.Cell>bathrooms</DataTable.Cell>
                          <DataTable.Cell>{place.living_place.bathrooms}</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                          <DataTable.Cell>water</DataTable.Cell>
                          <DataTable.Cell>{place.living_place.water}</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                          <DataTable.Cell>electricity</DataTable.Cell>
                          <DataTable.Cell>{place.living_place.electricity}</DataTable.Cell>
                        </DataTable.Row>
                      </DataTable>
                    :
                      (place.place_type == 3)?
                      <DataTable>

                        <DataTable.Row>
                          <DataTable.Cell>parking place</DataTable.Cell>
                          <DataTable.Cell>{place.office.parcking_place}</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                          <DataTable.Cell>square meters</DataTable.Cell>
                          <DataTable.Cell>{place.office.square_meters}</DataTable.Cell>
                        </DataTable.Row>

                      </DataTable>
                      :
                      (place.place_type == 4)?
                      <DataTable>

                        <DataTable.Row>
                          <DataTable.Cell>parking place</DataTable.Cell>
                          <DataTable.Cell>{place.hall.parcking_place}</DataTable.Cell>
                        </DataTable.Row>
                        <DataTable.Row>
                          <DataTable.Cell>square meters</DataTable.Cell>
                          <DataTable.Cell>{place.hall.square_meters}</DataTable.Cell>
                        </DataTable.Row>

                      </DataTable>
                      : null
                    }

          <View>

            <View style = {{ width: widthPercentageToDP("100%"), height: heightPercentageToDP('40%')}}>
              <MapView style={{ flex: 1 }}
                showsUserLocation={true}
                followUserLocation={true}
                zoomEnabled={true}
                provider="google"
                initialRegion={{
                  latitude: -6.815147,
                  longitude: 39.280119,

                }}>
                    <MapView.Marker
                        coordinate={{
                          latitude:-6.815147,
                          longitude: 39.280119
                        }}
                        title={"dit"}
                      />
                    </MapView>
            </View>

          </View>
          {
            (type == 'other') ?

              // the button should be displayed only if a place is not rented
              this.state.is_place_rented ?

              <Surface style={styles.surface}>
                  <Text>Unavailable</Text>
              </Surface> :
              this.state.is_loggedin ?
              <ShotemButton styleName="secondary" style={home_screen.rent_button} onPress={() => this.rentAplace(true, place.id)}>
                <Text style={{ color: 'white'}}> Rent this { place_type[place.place_type]()  } </Text>
              </ShotemButton>:

              <Surface style={styles.surface}>
                  <Text>Login to rent</Text>
              </Surface>

              :
              <View style={{flexDirection: 'row', marginLeft: widthPercentageToDP('5%')}}>

                <Appbar style={styles.appbar}>
                  <Appbar.Action icon="delete" onPress={() => this._showDialog()} />
                </Appbar>

                <View>
                  <Portal>
                    <Dialog
                      visible={this.state.visible}
                      onDismiss={this._hideDialog}>
                      <Dialog.Content>
                        <Paragraph>Are you sure?</Paragraph>
                      </Dialog.Content>
                      <Dialog.Actions>
                        <PaperButton onPress={this._hideDialog}><Text style={{color: 'black'}}>Cancel</Text></PaperButton>
                        <PaperButton onPress={this._hideDialog}><Text style={{color: 'black'}}>Yes</Text></PaperButton>
                      </Dialog.Actions>
                    </Dialog>
                  </Portal>
                </View>

              </View>
          }
        </View>

        {/* use snackbar to show indicate an action has been done */}
        <Snackbar
          visible={this.state.snackbar_visible}
          onDismiss={() => this.setState({ snackbar_visible: false })}
          style={styles.snackbar}
          theme = {_theme}
          duration = {1000}
          action={{
            label: 'Undo',
            onPress: () => {
              // undo the renting request
              this.rentAplace(false)
            },
          }}>
          { this.state.snackbar_message }
        </Snackbar>
      </ScrollView>


    )
  }
}

const styles = StyleSheet.create({
  heading: {
    padding: 20,
    paddingBottom: 0,
    color: "rgba(0, 0, 0, 0.8)",
    fontFamily: 'noto-bold'
  },
  subtitle: {
    color: "rgba(0, 0, 0, 0.4)",
    paddingLeft: 20,
    paddingBottom: heightPercentageToDP('3%')
  },
  description: {
    paddingTop: 0,
    paddingBottom: 3
  },
  short_info: {
    flexDirection: "row",
    alignItems: "center",
    justifyItems: "space-around"
  },
  short_info_content: {
    flexDirection: "column",
    justifyItems: "space-around"
  },
  tab_bar_style: {
    width: widthPercentageToDP("85%"),
    marginLeft: widthPercentageToDP("5%"),
    marginTop: heightPercentageToDP('2%')
  },
  tab: {
    backgroundColor: "white",
    fontWeight: "bold",
  },
  tab_text: {
    fontWeight: "bold",
  },
  details: {
    padding: 20,
    paddingTop: 10,
    color: "rgba(0, 0, 0, 0.7)",
    flexDirection: 'column'
  },
  container: {
    height: heightPercentageToDP("2%")
  },
  tab_underline_style: {
    borderRadius: 10,
    backgroundColor: 'rgb(0, 152, 95)'
  },
  snackbar: {
    width: widthPercentageToDP("70%"),
    marginLeft: widthPercentageToDP("15%"),
    bottom: widthPercentageToDP("5%")
  },
  surface: {
    padding: 8,
    height: heightPercentageToDP('5%'),
    width: widthPercentageToDP('30%'),
    marginLeft: widthPercentageToDP('35%'),
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
    color: '#0cc2aa'
  },
  appbar : {
    marginTop: 20,
    height: heightPercentageToDP('5%')
  },
});


export const _theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'rgba(0, 0, 0, .6)',
    accent: '#FFF',
  },
};
