import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet} from 'react-native';
import { Searchbar, Avatar, Button, Card, Title, Paragraph } from 'react-native-paper'
import { home_screen, theme } from '../Styles'
import API from '../api'

export default class Places extends React.Component {

  static navigationOptions = {
    header: null
  };

  
  constructor(props) {
    super(props)
    this.state = {
      places: []
    }
  }


  async componentDidMount() {
    const places = await API.makeGETrequest('places')
    this.setState({
      ...this.state,
      places: places
    })
  }


  render() { 
    return (
        <View style={home_screen.container}>
            <View style={ home_screen.search_bar_container }>
            <Avatar.Image size={34} source={require('../../../assets/images/avatar.jpg')} />
            <Searchbar 
                placeholder="search places"
                style={ home_screen.search_bar }
                theme={ theme }
            />
            </View> 
            <View style={home_screen.contect_view}>
            
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26, 26, 27, 0.1)"
  }
});