import React from 'react';
import { View, FlatList, Text, TouchableOpacity} from 'react-native';
import { Searchbar, Avatar, Button, Card, Title, Paragraph } from 'react-native-paper'
import { home_screen, theme } from '../Styles'
import API from '../api'
import { heightPercentageToDP } from 'react-native-responsive-screen';
import { image_root_url } from '../api'


export default class Place extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        place_info: {}
      };
    }



    render() {
      let place_type = {
        1: () => {return 'House'},
        2: () => {return 'Appartment'},
        3: () => {return 'Office'},
        4: () => {return 'Hall'}
      }

      let place_status = {
        1: () => {return 'New construction'},
        2: () => {return 'Good condition'},
        3: () => {return 'Old construction'},
      }

      const place = this.props._place;

      return (
        <Card style={[home_screen.home_cards, {fontFamily: 'noto-regular', fontWeight: 'bold'}]}>
          <Card.Title
            title={place.user.first_name + " " + place.user.last_name}
            titleStyle={[home_screen.card_title, {fontFamily: 'noto-bold'}]}
            subtitle={ place_type[place.place_type]() + " | " + place.location + " | " + place_status[place.status]() + " | " +

              ((place.place_type == 1)? place.living_place.price:
              (place.place_type == 3)? place.office.price:
              (place.place_type == 4)? place.hall.price: null)
              + " " +
              ((place.place_type == 1)? place.living_place.payment_terms:
              (place.place_type == 3)? place.office.payment_terms:
              (place.place_type == 4)? place.hall.payment_terms: null)

            }
            subtitleStyle={[home_screen.card_subtitle, {fontFamily: 'noto-regular'}]}
            left={props =>
              <Avatar.Image
                size={34}
                source={{ uri: image_root_url + place.user.userprofile.profile_picture }}
                style={{ marginTop: 0}}/>
            }

            style={home_screen.card_title_cover}/>
          <Card.Content style={home_screen.card_content}>
            <Card.Cover
              source={{ uri: image_root_url + place.cover_photo }}
              style={home_screen.card_cover}/>
            <Card.Content>
              <Paragraph style={home_screen.card_side_content}>
                <Title style={{fontFamily: 'noto-bold', color: 'rgba(0, 0, 0, .8)', fontSize: heightPercentageToDP('2.3%')}}>{place.title}...</Title>
              </Paragraph>
              <Paragraph style={[home_screen.card_paragraph, {fontFamily: 'noto-regular'}]}>
                { (place.place_type == 1)? place.living_place.descritpion:
                  (place.place_type == 3)? place.office.descritpion:
                  (place.place_type == 4)? place.hall.descritpion: null
                }
              </Paragraph>
            </Card.Content>
          </Card.Content>
        </Card>
      );
    }
  }
