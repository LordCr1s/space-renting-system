import { StyleSheet } from 'react-native'
import { DefaultTheme } from 'react-native-paper'
import { widthPercentageToDP as widthTodp, heightPercentageToDP as heightTodp } from 'react-native-responsive-screen'
import { Font } from 'expo';

const loadFonts = async () => {
    await Font.loadAsync({
        'ibm-regular': require('../../assets/fonts/IBMPlexSans-Regular.otf'),
      })
}


export const home_screen = StyleSheet.create({
    container : {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'rgb(0, 152, 95)',
        // backgroundColor: "#FFF",
        alignItems: 'center',       
    },
    place_details_container : {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: "#FFF",
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginTop: heightTodp('8.5%'),
        width: widthTodp('96%'),
        borderRadius: 8,
        marginBottom: heightTodp('1.5%'),
    },
    search_bar_container : {
        flex: 1,
        top: 30,
        backgroundColor: "rgb(0, 152, 95)",
        flexDirection: 'row',
    },
    search_bar: {
        width: widthTodp('80%'),
        shadowColor: '#FFF',
        backgroundColor: 'rgba(26, 26, 27, 0.1)',
        color: '#FFF',
        marginLeft: widthTodp('4%'),
        height: heightTodp('4.7%'),
        borderRadius: 15
    },
    contect_view: {
        flex: 8,
        backgroundColor: 'rgb(249, 249, 250)',
        width: widthTodp('100%'),
        alignItems: 'center',
    },
    home_cards: {
        marginTop: 4,
        width: widthTodp('100%'),
        padding: 0,
        backgroundColor: 'rgb(255, 255, 255) none repeat scroll 0% 0%',
        borderRadius: 0,
        elevation: 0,
    },
    card_title_cover: {
        height: heightTodp('6.5%'),
    },
    card_title: {
        color: 'rgba(0, 0, 0, 0.7)',
        fontSize: heightTodp('2%'),
        top: 4,
        marginLeft: -10
    },
    card_subtitle: {
        top: -4,
        fontSize: heightTodp('1.5%'),
        marginLeft: -10
    },
    card_header: {
        color: 'rgba(0, 0, 0, 0.7)',
        fontSize: heightTodp('2.3%'),
        top: 8,
        left: 5
    },
    card_cover: {
        width: widthTodp('30%'),
        height: heightTodp('15%'),
        borderRadius: 5
    },
    price_tag: {
        backgroundColor: '#00C851B3',
        color: 'white',
        paddingLeft: 300
    },
    card_paragraph: {
        color: 'rgba(0, 0, 0, 0.7)',
        paddingRight: 20,
        height: heightTodp('11.38%'),
        width: widthTodp('62%'),
        marginTop: heightTodp('-0.9%'),
    },
    card_upload_time: {
        color: 'rgba(0, 0, 0, .4)',
        paddingRight: 20,
        paddingLeft: 20,
        paddingBottom: 10
    },
    card_side_content: {
        color: 'rgba(0, 0, 0, .4)',
        fontSize: heightTodp('1%'),
        height: heightTodp('4%'),
        width: widthTodp('54%'),
    },
    card_content: {
        marginTop: heightTodp('.5%'),
        margin: 0, 
        flex: 1,
        flexDirection: 'row',
        padding: 0
    },
    car_number : {
        fontSize: heightTodp('2.5%'),
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    rent_button: {
        color: 'white',
        width: widthTodp('30%'),
        height: heightTodp('4.5%'),
        marginBottom: heightTodp('1.5%'),
        marginLeft: widthTodp('35%'),
        backgroundColor: 'rgb(0, 152, 95)',
        borderColor: 'rgb(0, 152, 95)',
        marginTop: 20
    }
});


export const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'rgb(0, 152, 95)',
      accent: '#FFF',
    },
  };