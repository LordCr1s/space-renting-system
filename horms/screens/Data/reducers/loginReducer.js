// ACTIONS TYPES DECLARATIONS
const SHOW_LOGIN_EMAIL_RESET ='login:emailReset'
const SHOW_RESET_CODE_SCREEN = 'login:showcodesreen'
const SHOW_PASSWORD_UPDATE_SCREEN = 'login:showUpdatePassword'

// creates an action that identifies what a user is requesting to be done
export const actionsCreator = (action, value) => ({
    type: action,
    payload: {
        // boolean value to determin visibility of a section
        // refer to Login component to understand this
        value: value 
    }
})

// provides email field to receive email address from the user for sending password reset code
const showLoginEmailReset = value => actionsCreator(SHOW_LOGIN_EMAIL_RESET, value)

// provides a field for a user to enter a code sent to their email
const showResetCodeScreen = value => actionsCreator(SHOW_RESET_CODE_SCREEN, value)

// provides passwords fields for user to setup their new password (reseting)
const showPasswordUpdateScreen = value => actionsCreator(SHOW_PASSWORD_UPDATE_SCREEN, value)

const loginPage = {
    // controls for the login sections visibility refer to login page to understand this
    viewControl: {
        isLoginPage: true,
        isPasswordResetemail: false,
        isPasswordResetCode: false,
        isPasswordReset: false
    },

    // link actions to the Login component
    actions : {
        showLoginEmailReset: showLoginEmailReset,
        showResetCodeScreen: showResetCodeScreen,
        showPasswordUpdateScreen: showPasswordUpdateScreen
    }
}

export const loginReducer = (state = loginPage, {
    type,
    payload
}) => {
    switch (type) {

        case SHOW_LOGIN_EMAIL_RESET:
            // on step one users provide their email to receive password reset code
            let setp_one = {...state.viewControl}
            setp_one.isPasswordResetemail = payload.value
            let stepOneState = {
                ...state,
                viewControl: setp_one
            }
            return stepOneState

        case SHOW_RESET_CODE_SCREEN:
            // on step two users provides code sent to the email they have provided at step one
            let step_two = {...state.viewControl}
            step_two.isPasswordResetCode = payload.value
            let stepTwoState = {
                ...state,
                viewControl: step_two
            }
            return stepTwoState

        case SHOW_PASSWORD_UPDATE_SCREEN:
            // on step three users creates new password
            let step_three = {...state.viewControl}
            step_three.isPasswordReset = payload.value
            let stepThreeState = {
                ...state,
                viewControl: step_three
            }
            return stepThreeState
            
        default:
            return state
    }
}