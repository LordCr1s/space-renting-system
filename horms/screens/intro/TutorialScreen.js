import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, Animated, Easing } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { Ionicons } from '@expo/vector-icons';
import Auth from '../authentication/Auth';

export default class TutorialScreen extends Component {

    state = {
        showAuthentication: false
    }

    static navigationOptions = {
      headerStyle: {
          shadowOpacity: 0,
          shadowColor: 'transparent',
          elevation: 0,
          borderBottomWidth:0,
      },
      header: null
    };

    _renderItem = (item) => {
        return (
          <View style={styles.slide}>
            <View style={styles.slideCard}>
                <Text style={styles.title}>{item.title}</Text>
                <Image style={styles.image} source={item.image} />
                <Text style={styles.text}>{item.text}</Text>
            </View>
          </View> 
        );
      }

      _renderNextButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Ionicons
              name="md-arrow-round-forward"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
      }

      _renderSkipButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Ionicons
              name="md-arrow-round-forward"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
      }

      _renderDoneButton = () => {
        return (
          <View style={[styles.buttonCircle, styles.buttonDone]}>
            <Ionicons
              name="md-checkmark"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
      }

      _onDone = () => {
        this.props.navigation.navigate('App');
      }

      _onSkip = () => {
        this.props.navigation.navigate('App');
      }

    render() {
        return <AppIntroSlider 
              renderDoneButton={this._renderDoneButton}
              renderNextButton={this._renderNextButton} 
              renderItem={this._renderItem}
              slides={slides}
              onDone={this._onDone}
              onSkip={this._onSkip}
              activeDotStyle={styles.activeDot}
              imageStyle={styles.image}/>
    }
}

const styles = StyleSheet.create({
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: '#00C851B3',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonDone : {
        backgroundColor: '#00C851B3', 
    },  
    image: {
        width: 400,
        height: 350,
        resizeMode: 'center',
    },
    slide: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    slideCard: {
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        padding: 20,
        width: 350,
        borderRadius: 20,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.3 
    },
    title: { 
        fontSize: 40,
        fontWeight: '300',
        paddingHorizontal: 16,
        color: 'rgba(0, 0, 0, .6)',
    },
    text: { 
        fontSize: 17,
        fontWeight: '300',
        paddingHorizontal: 16,
        color: 'rgba(0, 0, 0, .6)',
    },
    activeDot : {
        backgroundColor: '#00C851B3',
    }
  });

  const slides = [
    {
      key: '1',
      title: 'Places to live',
      text: '\t\t Rent or put appartments \nand houses for living with our application\n\t\t\tin a few minutes.',
      image: require('../../assets/images/home.png'),
      backgroundColor: '#FFFFFF',
    },
    {
      key: '2',
      title: 'Offices',
      text: '\t\tYou can rent or put \nbildings for offices on our application  \n\t\t\twith ease.',
      image: require('../../assets/images/office.png'),
      backgroundColor: '#FFFFFF',
    },
    {
      key: '3',
      title: 'No brokers',
      text: '\tIf you hated midle men, well! \nwith this application we provide direct\n \t   communication between \n\t\towner and renter.',
      image: require('../../assets/images/broker.png'),
      backgroundColor: '#F3F7F4',
    },
    {
      key: '4',
      title: 'Management',
      text: 'This app gives the owners of the buildngs a management tool to help them handle their buildngs and renters respectifully.',
      image: require('../../assets/images/management1.png'),
      backgroundColor: '#FEBE29',
    }
  ];