import React, { Component } from 'react';
import { StyleSheet, View, Text, Image} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class SplashScreen extends Component {
    
  render() {
    return (
        <View style={styles.SplashScreen_RootView}>
            <View style={styles.SplashScreen_ChildView}>
                <Image
                    source={require('../../assets/images/white_logo.png')}
                    style={styles.logo}
                    />
            </View>
            <Text style={styles.brand}>
                @2019 DIT, Inc.
            </Text>
        </View> 
    )
  }
}

const styles = StyleSheet.create({ 
    SplashScreen_RootView:
      {
          justifyContent: 'center',
          flex:1,
          position: 'absolute', 
          width: '100%',
          height: '100%',
      },
      SplashScreen_ChildView:
      {
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#FFFFFF',
          flex:1,
      },
      logo : {
        backgroundColor: 'transparent',
        shadowColor: '#000000', 
        width: 230,
        height: 230,
      }, 
      brand : {
        fontSize: 15,
        fontWeight: '300',
        paddingHorizontal: '38%',
        color: 'rgba(0, 0, 0, .6)',
        alignItems: 'center',
        position : 'absolute',
        bottom: 30
      }
  });