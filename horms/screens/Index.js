import React from 'react';
import HomeScreen from './Home/HomeScreen'
import PostScreen from './Home/PostScreen'
import ProfileScreen from './Home/ProfileScreen'
import Dashboard from './Home/Dashboard'
import { widthPercentageToDP as widthTodp, heightPercentageToDP as heightTodp } from 'react-native-responsive-screen'
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs"
import { Icon } from '@shoutem/ui'
import { AntDesign, MaterialIcons, Entypo } from '@expo/vector-icons'
import Auth from './authentication/Auth'

export default createMaterialBottomTabNavigator({
    home: { screen: HomeScreen,  },
    dashboard: { screen: Dashboard, },
    posts: { screen: PostScreen, },
    profile: { screen: Auth, },
  }, {
    initialRouteName: 'home',
    activeColor: 'rgba(0, 0, 0, .6)',
    inactiveColor: 'rgba(0, 0, 0, .4)',
    barStyle: { height: heightTodp('6.7%') },
  });


HomeScreen.navigationOptions = {
    title: 'Home',
    tabBarIcon: <MaterialIcons name="home" size={20} color={'rgba(0, 0, 0, .6)'} />
}

Dashboard.navigationOptions = {
    title: 'Dashboard',
    tabBarIcon: <MaterialIcons name="bookmark" size={20} color={'rgba(0, 0, 0, .6)'}/>
}

PostScreen.navigationOptions = {
    title: 'My Posts',
    tabBarIcon: <AntDesign name="star" size={20} color={'rgba(0, 0, 0, .6)'}/>
}

ProfileScreen.navigationOptions = {
    title: 'Profile',
    tabBarIcon: <Entypo name="user" size={20} color={'rgba(0, 0, 0, .6)'}/>
}
