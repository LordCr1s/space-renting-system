import { AsyncStorage } from 'react-native'

export const userHasLoggedIn = async (user) => {

  try {
      await AsyncStorage.setItem('user', JSON.stringify(user));
      console.log('user has logged in')
    } catch (error) {
      // Error saving data
    }
}

export const userHasLogedOut = async (user) => {

  try {
      await AsyncStorage.removeItem('user');
      console.log('logging out user')
    } catch (error) {
      // Error saving data
    }
}

export const getUserEmail = async () => {
  console.log('cheking getasdf')
  let user = '';
  try {
    user = await AsyncStorage.getItem('user') || 'none';
    if (user != 'none') {
      return user
    } else {
      return 'none'
    }
  } catch (error) {
    console.log(error.message);
  }
}

export const isAuthenticated = async () => {
  console.log('cheking authentication')
  let user = '';
  try {
    user = await AsyncStorage.getItem('user') || 'none';
    if (user != 'none') {
      return true
    } else {
      return false
    }
  } catch (error) {
    console.log(error.message);
  }
}
